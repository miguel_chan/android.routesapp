package merida.tec.routesapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import merida.tec.routesapp.R;
import merida.tec.routesapp.controllers.UserController;
import merida.tec.routesapp.fragments.LogInFragment;
import merida.tec.routesapp.utils.FragmentHandler;
import merida.tec.routesapp.utils.ObjectUtils;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 30/05/16.
 */
public class LogInActivity extends RoboActionBarActivity implements FragmentHandler{

    private static final String TAG = LogInActivity.class.getSimpleName();

    @InjectView(R.id.toolbar)
    private Toolbar mToolbar;
    @Inject
    private UserController mUserController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mayLoadMainActivity();

        configureToolbar();

        if(ObjectUtils.isNull(savedInstanceState)) {
            addFragment();
        }
    }

    @Override
    public void addFragment(Fragment fragment, String tag, boolean addToBackStack){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.framelayout_main_fragmentcontainer, fragment, tag);
        if(addToBackStack){
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    private void addFragment(){
        Fragment aFragment = LogInFragment
                .Builder
                .newBuilder()
                .build();
        addFragment(aFragment, LogInFragment.TAG, false);
    }

    private void mayLoadMainActivity(){
        if(mUserController.isLoggedIn()){
            Intent intent = MainActivity
                    .Builder
                    .newBuilder()
                    .build(this);

            intent = IntentCompat.makeRestartActivityTask(intent.getComponent());
            startActivity(intent);
        }
    }

    private void configureToolbar(){
        setSupportActionBar(mToolbar);
    }
}

package merida.tec.routesapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import merida.tec.routesapp.R;
import merida.tec.routesapp.fragments.MainFragment;
import merida.tec.routesapp.utils.FragmentHandler;
import merida.tec.routesapp.utils.ObjectUtils;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 31/05/16.
 */
public class MainActivity extends RoboActionBarActivity implements FragmentHandler {

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public Intent build(Context context){
            Intent intent = new Intent(context, MainActivity.class);
            return intent;
        }

        public void buildAndStart(Context context){
            Intent intent = build(context);
            context.startActivity(intent);
        }
    }

    @Override
    public void addFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.framelayout_main_fragmentcontainer, fragment, tag);
        if(addToBackStack){
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureToolbar();
        configureFragmentManager();

        if(ObjectUtils.isNull(savedInstanceState)){
            addMainFragment();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deconfigureFragmentManager();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(itemId == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void deconfigureFragmentManager(){
        getSupportFragmentManager().removeOnBackStackChangedListener(mOnBackStackChangedListener);
    }

    private void configureFragmentManager(){
        getSupportFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);

        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void addMainFragment(){
        MainFragment mainFragment = MainFragment
                .Builder
                .newBuilder()
                .build();
        addFragment(mainFragment, MainFragment.TAG, false);
    }

    private void configureToolbar(){
        setSupportActionBar(mToolbar);
    }

    private FragmentManager.OnBackStackChangedListener mOnBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            if(getSupportFragmentManager().getBackStackEntryCount() > 0){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            }else{
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            }
        }
    };

    @InjectView(R.id.toolbar)
    private Toolbar mToolbar;
}

package merida.tec.routesapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import merida.tec.routesapp.R;
import merida.tec.routesapp.adapters.holders.RouteHolder;
import merida.tec.routesapp.models.Route;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.utils.OnViewHolderClickListener;

/**
 * Created by miguelchan on 05/06/16.
 */
public class RoutesAdapter extends RecyclerView.Adapter<RouteHolder> {

    public interface OnRouteClickListener {
        void onRouteClick(Route route, View view, int position);
    }

    private OnRouteClickListener mOnRouteClickListener;
    private List<Route> mRoutes;

    public RoutesAdapter(List<Route> routes){
        mRoutes = routes;
    }

    public void setOnRouteClickListener(OnRouteClickListener onRouteClickListener){
        mOnRouteClickListener = onRouteClickListener;
    }

    @Override
    public RouteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mainView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new RouteHolder(mainView, mOnViewHolderClickListener);
    }

    @Override
    public void onBindViewHolder(RouteHolder holder, int position) {
        Route currentRoute = mRoutes.get(position);
        holder.configure(currentRoute);
    }

    @Override
    public int getItemCount() {
        return mRoutes.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_route;
    }

    private OnViewHolderClickListener mOnViewHolderClickListener = new OnViewHolderClickListener() {
        @Override
        public void onViewHolderClick(View view, int position) {
            if(ObjectUtils.isNotNull(mOnRouteClickListener)){
                Route aRoute = mRoutes.get(position);
                mOnRouteClickListener.onRouteClick(aRoute, view, position);
            }
        }
    };
}

package merida.tec.routesapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import merida.tec.routesapp.R;
import merida.tec.routesapp.adapters.holders.UserHolder;
import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 31/05/16.
 */
public class UsersAdapter extends RecyclerView.Adapter<UserHolder> {

    public UsersAdapter(List<User> users){
        mUsers = users;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mainView = LayoutInflater.from(parent.getContext()).inflate(viewType, null, false);
        return new UserHolder(mainView);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        User currentUser = mUsers.get(position);
        holder.configure(currentUser);
    }

    public User getUser(int position){
        return mUsers.get(position);
    }

    public User deleteUser(int position){
        User anUser = mUsers.remove(position);
        notifyItemRemoved(position);
        return anUser;
    }

    public void addUser(int position, User user){
        mUsers.add(position, user);
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_user;
    }

    private List<User> mUsers;
}

package merida.tec.routesapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import merida.tec.routesapp.R;
import merida.tec.routesapp.adapters.holders.VehicleHolder;
import merida.tec.routesapp.models.Vehicle;

/**
 * Created by miguelchan on 31/05/16.
 */
public class VehiclesAdapter extends RecyclerView.Adapter<VehicleHolder> {

    private List<Vehicle> mVehicleList;

    public VehiclesAdapter(List<Vehicle> vehicles){
        mVehicleList = vehicles;
    }

    @Override
    public VehicleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mainView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new VehicleHolder(mainView);
    }

    @Override
    public void onBindViewHolder(VehicleHolder holder, int position) {
        Vehicle currentVehicle = mVehicleList.get(position);
        holder.configure(currentVehicle);
    }

    @Override
    public int getItemCount() {
        return mVehicleList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_vehicle;
    }
}

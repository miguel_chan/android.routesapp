package merida.tec.routesapp.adapters.holders;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import merida.tec.routesapp.R;
import merida.tec.routesapp.models.Route;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.utils.OnViewHolderClickListener;
import merida.tec.routesapp.utils.ViewUtils;

/**
 * Created by miguelchan on 05/06/16.
 */
public class RouteHolder extends RecyclerView.ViewHolder{

    private AppCompatTextView mRouteName;
    private OnViewHolderClickListener mOnViewHolderClickListener;

    public RouteHolder(View itemView, OnViewHolderClickListener onViewHolderClickListener) {
        super(itemView);
        configureView();
        mOnViewHolderClickListener = onViewHolderClickListener;
    }

    public void configure(Route route){
        String routeName = route.getRouteName();
        mRouteName.setText(routeName);
    }

    private void configureView(){
        mRouteName = ViewUtils.findViewById(itemView, R.id.row_route_routename);

        itemView.setOnClickListener(mOnClickListener);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(ObjectUtils.isNotNull(mOnViewHolderClickListener)){
                mOnViewHolderClickListener.onViewHolderClick(itemView, getAdapterPosition());
            }
        }
    };
}

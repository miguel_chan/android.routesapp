package merida.tec.routesapp.adapters.holders;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;

import merida.tec.routesapp.R;
import merida.tec.routesapp.models.User;
import merida.tec.routesapp.utils.ViewUtils;

/**
 * Created by miguelchan on 31/05/16.
 */
public class UserHolder extends RecyclerView.ViewHolder {

    private AppCompatTextView mUserName;
    private AppCompatTextView mPhoneNumber;
    private SimpleDraweeView mImageView;

    public UserHolder(View itemView) {
        super(itemView);
        configureView();
    }

    public void configure(User user){
        String phoneNumber = user.getPhoneNumber();
        String email = user.getEmail();

        mUserName.setText(email);
        mPhoneNumber.setText(phoneNumber);
    }

    private void configureView(){
        mUserName = ViewUtils.findViewById(itemView, R.id.appcompattextview_username);
        mPhoneNumber = ViewUtils.findViewById(itemView, R.id.appcompattextview_phonenumber);
        mImageView = ViewUtils.findViewById(itemView, R.id.simpledraweeview_userimage);
    }
}

package merida.tec.routesapp.adapters.holders;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import merida.tec.routesapp.R;
import merida.tec.routesapp.models.Vehicle;
import merida.tec.routesapp.utils.ViewUtils;

/**
 * Created by miguelchan on 31/05/16.
 */
public class VehicleHolder extends RecyclerView.ViewHolder {

    private AppCompatTextView mVehicleBrand;
    private AppCompatTextView mVehiclePlates;
    private AppCompatTextView mAcceptsChildren;

    public VehicleHolder(View itemView) {
        super(itemView);
        configureView();
    }

    public void configure(Vehicle vehicle){
        String brand = vehicle.getBrand();
        String plates = vehicle.getPlates();

        mVehicleBrand.setText(brand);
        mVehiclePlates.setText(plates);

        if(vehicle.isAcceptsChildren()){
            mAcceptsChildren.setText("Acepta Niños");
        }else{
            mAcceptsChildren.setText("No Acepta Niños");
        }
    }

    private void configureView(){
        mVehicleBrand = ViewUtils.findViewById(itemView, R.id.appcompattextview_vehiclebrand);
        mVehiclePlates = ViewUtils.findViewById(itemView, R.id.appcompattextview_vehicleplates);
        mAcceptsChildren = ViewUtils.findViewById(itemView, R.id.appcomptextview_acceptschildren);
    }
}

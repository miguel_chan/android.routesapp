package merida.tec.routesapp.controllers;

import java.util.List;

import javax.inject.Inject;

import merida.tec.routesapp.database.commands.AddRouteCommand;
import merida.tec.routesapp.database.handlers.commands.AddRouteCommandHandler;
import merida.tec.routesapp.database.handlers.queries.FindRoutesQueryHandler;
import merida.tec.routesapp.database.queries.FindRoutesQuery;
import merida.tec.routesapp.models.Coordinate;
import merida.tec.routesapp.models.Route;
import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 03/06/16.
 */
public class RoutesController {

    @Inject
    public RoutesController(AddRouteCommandHandler addRouteCommandHandler,
                            FindRoutesQueryHandler findRoutesQueryHandler,
                            UserController userController){
        mAddRouteCommandHandler = addRouteCommandHandler;
        mFindRoutesQueryHandler = findRoutesQueryHandler;
        mUserController = userController;
    }

    public boolean addRoute(Coordinate startLocation,
                            Coordinate endLocation,
                            String encodedPolyline,
                            String name){
        AddRouteCommand addRouteCommand = new AddRouteCommand(
                startLocation,
                endLocation, encodedPolyline,
                mUserController.getCurrentUser().getId(),
                name);

        AddRouteCommand.CommandResult commandResult = mAddRouteCommandHandler.handle(addRouteCommand);
        return commandResult.isSuccess();
    }

    public List<Route> getRoutes(User user){
        FindRoutesQuery findRoutesQuery = new FindRoutesQuery(user.getId());
        FindRoutesQuery.QueryResult queryResult = mFindRoutesQueryHandler.handle(findRoutesQuery);
        return queryResult.getRoutes();
    }

    public List<Route> getRoutes(){
        User user = mUserController.getCurrentUser();
        return getRoutes(user);
    }

    private UserController mUserController;
    private AddRouteCommandHandler mAddRouteCommandHandler;
    private FindRoutesQueryHandler mFindRoutesQueryHandler;
}

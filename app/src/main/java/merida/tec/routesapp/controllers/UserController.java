package merida.tec.routesapp.controllers;

import android.support.annotation.Nullable;

import org.roboguice.shaded.goole.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

import merida.tec.routesapp.database.commands.InsertUserCommand;
import merida.tec.routesapp.database.handlers.queries.GetUserQueryHandler;
import merida.tec.routesapp.database.handlers.commands.InsertUserCommandHandler;
import merida.tec.routesapp.database.queries.GetUserQuery;
import merida.tec.routesapp.models.User;
import merida.tec.routesapp.utils.ListUtils;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.requests.CreateUserRequest;
import merida.tec.routesapp.web.requests.GetUserRequest;
import merida.tec.routesapp.web.requests.GetUsersRequest;
import merida.tec.routesapp.web.requests.LogInRequest;
import merida.tec.routesapp.web.responses.CreateUserResponse;
import merida.tec.routesapp.web.responses.GetUserResponse;
import merida.tec.routesapp.web.responses.GetUsersResponse;
import merida.tec.routesapp.web.responses.LogInResponse;

/**
 * Created by miguelchan on 30/05/16.
 */
public class UserController {

    private WebServiceClient mWebServiceClient;
    private InsertUserCommandHandler mInsertUserCommandHandler;
    private GetUserQueryHandler mGetUserQueryHandler;

    @Inject
    public UserController(WebServiceClient webServiceClient,
                          InsertUserCommandHandler insertUserCommandHandler,
                          GetUserQueryHandler getUserQueryHandler){
        mWebServiceClient = webServiceClient;
        mInsertUserCommandHandler = insertUserCommandHandler;
        mGetUserQueryHandler = getUserQueryHandler;
    }

    @Nullable
    public User getCurrentUser(){
        GetUserQuery getUserQuery = new GetUserQuery();
        return mGetUserQueryHandler.handle(getUserQuery);
    }

    public List<User> getUsers(){
        GetUsersRequest request = new GetUsersRequest(100, 1);
        GetUsersResponse response = mWebServiceClient.getUsers(request);

        if(ObjectUtils.isNotNull(response) && ListUtils.isValidList(response.getUsers())){
            return response.getUsers();
        }

        return Lists.newArrayList();
    }

    public boolean createUser(User user){
        CreateUserRequest request = new CreateUserRequest(
                user.getPhoneNumber(),
                user.getEmail(),
                user.getPassword(),
                user.getGender(),
                null
        );

        CreateUserResponse response = mWebServiceClient.createUse(request);

        if(ObjectUtils.isNotNull(response)){
            return response.getId() > 0;
        }
        return false;
    }

    public boolean isLoggedIn(){
        GetUserQuery query = new GetUserQuery();
        return mGetUserQueryHandler.handle(query) != null;
    }

    public boolean logIn(String userName, String password){
        LogInRequest request = new LogInRequest(userName, password);
        LogInResponse response = mWebServiceClient.logIn(request);

        if (ObjectUtils.isNull(response)) {
            return false;
        }
        int id = response.getId();
        GetUserRequest getUserRequest = new GetUserRequest(id);
        GetUserResponse getUserResponse = mWebServiceClient.getUser(getUserRequest);

        if(ObjectUtils.isNotNull(getUserResponse) && ObjectUtils.isNotNull(getUserResponse.getUser())){
            User user = getUserResponse.getUser();
            InsertUserCommand command = new InsertUserCommand(user);
            try {
                mInsertUserCommandHandler.handle(command);
                return true;
            }catch(IllegalArgumentException e){
                return false;
            }
        }
        return false;
    }
}

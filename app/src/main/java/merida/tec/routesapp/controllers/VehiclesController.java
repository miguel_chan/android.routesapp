package merida.tec.routesapp.controllers;

import org.roboguice.shaded.goole.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

import merida.tec.routesapp.database.commands.AddVehicleCommand;
import merida.tec.routesapp.database.commands.DeleteVehicleCommand;
import merida.tec.routesapp.database.handlers.commands.AddVehicleCommandHandler;
import merida.tec.routesapp.database.handlers.commands.DeleteVehicleCommandHandler;
import merida.tec.routesapp.models.User;
import merida.tec.routesapp.models.Vehicle;
import merida.tec.routesapp.utils.ListUtils;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.requests.GetUserVehiclesRequest;
import merida.tec.routesapp.web.responses.CreateUserVehicleResponse;
import merida.tec.routesapp.web.responses.GetUserVehiclesResponse;

/**
 * Created by miguelchan on 31/05/16.
 */
public class VehiclesController {

    private UserController mUserController;
    private WebServiceClient mWebServiceClient;
    private AddVehicleCommandHandler mAddVehicleCommandHandler;
    private DeleteVehicleCommandHandler mDeleteVehicleCommandHandler;

    @Inject
    public VehiclesController(UserController userController,
                              WebServiceClient webServiceClient,
                              AddVehicleCommandHandler addVehicleCommandHandler,
                              DeleteVehicleCommandHandler deleteVehicleCommandHandler){
        mUserController = userController;
        mWebServiceClient = webServiceClient;
        mAddVehicleCommandHandler = addVehicleCommandHandler;
        mDeleteVehicleCommandHandler = deleteVehicleCommandHandler;
    }

    public boolean addVehicle(User user, String brand, String plates, boolean acceptsChildren){
        AddVehicleCommand command = new AddVehicleCommand(user, plates, brand, acceptsChildren);
        CreateUserVehicleResponse response = mAddVehicleCommandHandler.handle(command);
        return response.getId() > 0;
    }

    public boolean deleteVehicle(User user, Vehicle vehicle){
        DeleteVehicleCommand command = new DeleteVehicleCommand(vehicle.getId(), user.getId());
        DeleteVehicleCommand.CommandResult result = mDeleteVehicleCommandHandler.handle(command);
        return result.isSuccess();
    }

    public boolean deleteVehicle(Vehicle vehicle){
        User user = mUserController.getCurrentUser();
        return deleteVehicle(user, vehicle);
    }

    public boolean addVehicle(String brand, String plates, boolean acceptsChildren){
        User currentUser = mUserController.getCurrentUser();
        return addVehicle(currentUser, brand, plates, acceptsChildren);
    }

    public List<Vehicle> getVehicles(User user){
        List<Vehicle> vehicles = Lists.newArrayList();

        if(ObjectUtils.isNotNull(user)) {
            GetUserVehiclesRequest request = new GetUserVehiclesRequest(user.getId(), 100, 1);
            GetUserVehiclesResponse response = mWebServiceClient.getUserVehicles(request);

            if (ObjectUtils.isNotNull(response) && ListUtils.isValidList(response.getVehicles())) {
                vehicles = response.getVehicles();
            }
        }

        return vehicles;
    }

    public List<Vehicle> getVehicles(){
        User currentUser = mUserController.getCurrentUser();
        return getVehicles(currentUser);
    }

}

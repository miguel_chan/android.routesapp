package merida.tec.routesapp.database.commands;

import merida.tec.routesapp.models.Coordinate;

/**
 * Created by miguelchan on 03/06/16.
 */
public class AddRouteCommand implements Command<AddRouteCommand.CommandResult>{

    public static class CommandResult {

        public CommandResult(boolean success, String message) {
            this.success = success;
            this.message = message;
        }

        public boolean isSuccess() {
            return success;
        }

        public String getMessage() {
            return message;
        }

        private boolean success;
        private String message;
    }

    public AddRouteCommand(Coordinate startLocation,
                           Coordinate endLocation,
                           String encodedPolyline,
                           int userId,
                           String name) {
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.encodedPolyline = encodedPolyline;
        this.userId = userId;
        this.name = name;
    }

    public Coordinate getStartLocation() {
        return startLocation;
    }

    public Coordinate getEndLocation() {
        return endLocation;
    }

    public String getEncodedPolyline() {
        return encodedPolyline;
    }

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    private Coordinate startLocation;
    private Coordinate endLocation;
    private String encodedPolyline;
    private int userId;
    private String name;
}

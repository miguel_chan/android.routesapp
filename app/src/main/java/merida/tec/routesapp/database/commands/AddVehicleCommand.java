package merida.tec.routesapp.database.commands;

import merida.tec.routesapp.models.User;
import merida.tec.routesapp.web.responses.CreateUserVehicleResponse;

/**
 * Created by miguelchan on 31/05/16.
 */
public class AddVehicleCommand implements Command<CreateUserVehicleResponse> {

    public AddVehicleCommand(User user, String plates, String brand, boolean acceptsChildren) {
        this.user = user;
        this.plates = plates;
        this.brand = brand;
        this.acceptsChildren = acceptsChildren;
    }

    public String getPlates() {
        return plates;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isAcceptsChildren() {
        return acceptsChildren;
    }

    public User getUser() {
        return user;
    }

    private User user;
    private String plates;
    private String brand;
    private boolean acceptsChildren;
}

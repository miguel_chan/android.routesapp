package merida.tec.routesapp.database.commands;

/**
 * Created by miguelchan on 03/06/16.
 */
public class DeleteVehicleCommand implements Command<DeleteVehicleCommand.CommandResult> {

    public static class CommandResult {

        public CommandResult(boolean success, String message) {
            this.success = success;
            this.message = message;
        }

        public boolean isSuccess() {
            return success;
        }

        public String getMessage() {
            return message;
        }

        private boolean success;
        private String message;
    }

    public DeleteVehicleCommand(int vehicleId, int userId) {
        this.vehicleId = vehicleId;
        this.userId = userId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public int getUserId() {
        return userId;
    }

    private int vehicleId;
    private int userId;
}

package merida.tec.routesapp.database.commands;

import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 30/05/16.
 */
public class InsertUserCommand implements Command<Integer> {

    public InsertUserCommand(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    private User user;
}

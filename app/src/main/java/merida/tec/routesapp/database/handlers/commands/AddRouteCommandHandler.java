package merida.tec.routesapp.database.handlers.commands;

import merida.tec.routesapp.database.commands.AddRouteCommand;

/**
 * Created by miguelchan on 03/06/16.
 */
public interface AddRouteCommandHandler
        extends CommandHandler<AddRouteCommand.CommandResult, AddRouteCommand>{
}

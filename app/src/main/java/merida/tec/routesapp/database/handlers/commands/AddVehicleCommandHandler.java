package merida.tec.routesapp.database.handlers.commands;

import merida.tec.routesapp.database.commands.AddVehicleCommand;
import merida.tec.routesapp.web.responses.CreateUserVehicleResponse;

/**
 * Created by miguelchan on 31/05/16.
 */
public interface AddVehicleCommandHandler extends CommandHandler<CreateUserVehicleResponse, AddVehicleCommand>{
}

package merida.tec.routesapp.database.handlers.commands;

import merida.tec.routesapp.database.commands.Command;

/**
 * Created by miguelchan on 30/05/16.
 */
public interface CommandHandler<TResult, TCommand extends Command<TResult>> {

    TResult handle(TCommand command);

}

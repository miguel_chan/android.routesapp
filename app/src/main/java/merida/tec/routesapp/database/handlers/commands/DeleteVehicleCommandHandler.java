package merida.tec.routesapp.database.handlers.commands;

import merida.tec.routesapp.database.commands.DeleteVehicleCommand;

/**
 * Created by miguelchan on 03/06/16.
 */
public interface DeleteVehicleCommandHandler
        extends CommandHandler<DeleteVehicleCommand.CommandResult, DeleteVehicleCommand>{
}

package merida.tec.routesapp.database.handlers.commands;

import merida.tec.routesapp.database.commands.InsertUserCommand;
import merida.tec.routesapp.database.handlers.commands.CommandHandler;

/**
 * Created by miguelchan on 30/05/16.
 */
public interface InsertUserCommandHandler extends CommandHandler<Integer, InsertUserCommand> {

}

package merida.tec.routesapp.database.handlers.impl;

import javax.inject.Inject;

import merida.tec.routesapp.database.commands.AddRouteCommand;
import merida.tec.routesapp.database.handlers.commands.AddRouteCommandHandler;
import merida.tec.routesapp.models.Coordinate;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.requests.AddRouteRequest;
import merida.tec.routesapp.web.responses.AddRouteResponse;

/**
 * Created by miguelchan on 03/06/16.
 */
public class BaseAddRouteCommandHandler implements AddRouteCommandHandler {

    private WebServiceClient mWebServiceClient;

    @Inject
    public BaseAddRouteCommandHandler(WebServiceClient webServiceClient){
        mWebServiceClient = webServiceClient;
    }

    @Override
    public AddRouteCommand.CommandResult handle(AddRouteCommand command) {
        int userId = command.getUserId();
        Coordinate startCoordinate = command.getStartLocation();
        Coordinate endCoordinate = command.getEndLocation();
        String polyline = command.getEncodedPolyline();
        String name = command.getName();

        AddRouteRequest request = new AddRouteRequest(
                userId,
                name,
                startCoordinate,
                endCoordinate,
                polyline
        );

        AddRouteResponse response = mWebServiceClient.addRoute(request);

        AddRouteCommand.CommandResult result;
        if(ObjectUtils.isNotNull(response)){
            result = new AddRouteCommand.CommandResult(response.getId() > 0, response.getMessage());
        }else{
            result = new AddRouteCommand.CommandResult(false, "Error al Crear la Ruta");
        }

        return result;
    }
}

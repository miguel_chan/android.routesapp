package merida.tec.routesapp.database.handlers.impl;

import javax.inject.Inject;

import merida.tec.routesapp.database.commands.AddVehicleCommand;
import merida.tec.routesapp.database.handlers.commands.AddVehicleCommandHandler;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.requests.CreateUserVehicleRequest;
import merida.tec.routesapp.web.responses.CreateUserVehicleResponse;

/**
 * Created by miguelchan on 31/05/16.
 */
public class BaseAddVehicleCommandHandler implements AddVehicleCommandHandler{

    private WebServiceClient mClient;

    @Inject
    public BaseAddVehicleCommandHandler(WebServiceClient webServiceClient){
        mClient = webServiceClient;
    }

    @Override
    public CreateUserVehicleResponse handle(AddVehicleCommand command) {
        String plates = command.getPlates();
        String brand = command.getBrand();
        boolean accepts = command.isAcceptsChildren();
        int userId = command.getUser().getId();

        CreateUserVehicleRequest request = new CreateUserVehicleRequest(plates, brand, accepts, userId);
        CreateUserVehicleResponse response = mClient.createUserVehicle(request);

        if(ObjectUtils.isNull(response)){
            response = new CreateUserVehicleResponse("Error Creating Vehicle", -1);
        }

        return response;
    }
}

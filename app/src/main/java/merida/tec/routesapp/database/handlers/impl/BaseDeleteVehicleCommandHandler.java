package merida.tec.routesapp.database.handlers.impl;

import javax.inject.Inject;

import merida.tec.routesapp.database.commands.DeleteVehicleCommand;
import merida.tec.routesapp.database.handlers.commands.DeleteVehicleCommandHandler;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.requests.DeleteVehicleRequest;
import merida.tec.routesapp.web.responses.DeleteVehicleResponse;

/**
 * Created by miguelchan on 03/06/16.
 */
public class BaseDeleteVehicleCommandHandler implements DeleteVehicleCommandHandler {

    private WebServiceClient mWebServiceClient;

    @Inject
    public BaseDeleteVehicleCommandHandler(WebServiceClient webServiceClient){
        mWebServiceClient = webServiceClient;
    }

    @Override
    public DeleteVehicleCommand.CommandResult handle(DeleteVehicleCommand command) {
        int userId = command.getUserId();
        int vehicleId = command.getVehicleId();

        DeleteVehicleRequest request = new DeleteVehicleRequest(userId, vehicleId);
        DeleteVehicleResponse response = mWebServiceClient.deleteVehicle(request);

        DeleteVehicleCommand.CommandResult commandResult;
        if(ObjectUtils.isNotNull(response)){
            commandResult = new DeleteVehicleCommand
                    .CommandResult(response.isSuccess(), response.getMessage());
        }else{
            commandResult = new DeleteVehicleCommand
                    .CommandResult(false, "Error al borrar el Vehículo");
        }

        return commandResult;
    }
}

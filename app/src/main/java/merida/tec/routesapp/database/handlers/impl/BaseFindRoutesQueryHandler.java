package merida.tec.routesapp.database.handlers.impl;

import com.google.appengine.repackaged.com.google.api.client.util.Lists;

import javax.inject.Inject;

import merida.tec.routesapp.database.handlers.queries.FindRoutesQueryHandler;
import merida.tec.routesapp.database.queries.FindRoutesQuery;
import merida.tec.routesapp.models.Route;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.requests.GetUserRoutesRequest;
import merida.tec.routesapp.web.responses.GetUserRoutesResponse;

/**
 * Created by miguelchan on 03/06/16.
 */
public class BaseFindRoutesQueryHandler implements FindRoutesQueryHandler {

    private WebServiceClient mWebServiceClient;

    @Inject
    public BaseFindRoutesQueryHandler(WebServiceClient webServiceClient){
        mWebServiceClient = webServiceClient;
    }

    @Override
    public FindRoutesQuery.QueryResult handle(FindRoutesQuery query) {
        int userId = query.getUserId();

        GetUserRoutesRequest request = new GetUserRoutesRequest(userId);
        GetUserRoutesResponse response = mWebServiceClient.getUserRoutes(request);

        FindRoutesQuery.QueryResult queryResult;
        if(ObjectUtils.isNotNull(response)){
            queryResult = new FindRoutesQuery.QueryResult(response.getRoutes());
        }else{
            queryResult = new FindRoutesQuery.QueryResult(Lists.<Route>newArrayList());
        }

        return queryResult;
    }

}

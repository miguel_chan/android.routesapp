package merida.tec.routesapp.database.handlers.impl;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import merida.tec.routesapp.database.handlers.queries.GetUserQueryHandler;
import merida.tec.routesapp.database.ormlite.DatabaseHelper;
import merida.tec.routesapp.database.queries.GetUserQuery;
import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 30/05/16.
 */
public class BaseGetUserQueryHandler implements GetUserQueryHandler {

    private DatabaseHelper mHelper;

    @Inject
    public BaseGetUserQueryHandler(DatabaseHelper databaseHelper){
        mHelper = databaseHelper;
    }

    @Override
    public User handle(GetUserQuery query) {
        try {
            Dao<User, Integer> dao = mHelper.getDao(User.class);
            List<User> allUsers = dao.queryForAll();
            if(allUsers != null && allUsers.size() > 0) {
                return allUsers.get(0);
            }
        } catch (SQLException e) {
        }

        return null;
    }
}

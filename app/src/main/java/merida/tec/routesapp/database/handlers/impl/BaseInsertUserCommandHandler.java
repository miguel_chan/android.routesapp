package merida.tec.routesapp.database.handlers.impl;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import javax.inject.Inject;

import merida.tec.routesapp.database.commands.InsertUserCommand;
import merida.tec.routesapp.database.handlers.commands.InsertUserCommandHandler;
import merida.tec.routesapp.database.ormlite.DatabaseHelper;
import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 30/05/16.
 */
public class BaseInsertUserCommandHandler implements InsertUserCommandHandler {

    @Inject
    public BaseInsertUserCommandHandler(DatabaseHelper databaseHelper){
        mHelper = databaseHelper;
    }

    @Override
    public Integer handle(InsertUserCommand command) {
        User user = command.getUser();

        try {
            Dao<User, Integer> dao = mHelper.getDao(User.class);
            if(dao.idExists(user.getId())){
                dao.update(user);
            }else{
                dao.create(user);
            }
            return 1;
        } catch (SQLException e) {
            throw new IllegalArgumentException("Error Creating User Object");
        }
    }

    private DatabaseHelper mHelper;
}

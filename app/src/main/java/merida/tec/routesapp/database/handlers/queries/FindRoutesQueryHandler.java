package merida.tec.routesapp.database.handlers.queries;

import merida.tec.routesapp.database.queries.FindRoutesQuery;

/**
 * Created by miguelchan on 03/06/16.
 */
public interface FindRoutesQueryHandler
        extends QueryHandler<FindRoutesQuery.QueryResult, FindRoutesQuery>{
}

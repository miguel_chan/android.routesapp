package merida.tec.routesapp.database.handlers.queries;

import merida.tec.routesapp.database.queries.GetUserQuery;
import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 30/05/16.
 */
public interface GetUserQueryHandler extends QueryHandler<User, GetUserQuery>{
}

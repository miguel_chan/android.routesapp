package merida.tec.routesapp.database.handlers.queries;

import merida.tec.routesapp.database.queries.Query;

/**
 * Created by miguelchan on 30/05/16.
 */
public interface QueryHandler<TResult, TQuery extends Query<TResult>> {

    TResult handle(TQuery query);

}

package merida.tec.routesapp.database.queries;

import java.util.List;

import merida.tec.routesapp.models.Route;

/**
 * Created by miguelchan on 03/06/16.
 */
public class FindRoutesQuery implements Query<FindRoutesQuery.QueryResult>{

    public static class QueryResult {

        public QueryResult(List<Route> routes) {
            this.routes = routes;
        }

        public List<Route> getRoutes() {
            return routes;
        }

        private List<Route> routes;
    }

    public FindRoutesQuery(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    private int userId;
}

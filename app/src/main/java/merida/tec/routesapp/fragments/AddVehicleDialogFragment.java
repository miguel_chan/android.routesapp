package merida.tec.routesapp.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import merida.tec.routesapp.R;
import merida.tec.routesapp.services.VehiclesService;
import roboguice.fragment.RoboDialogFragment;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 31/05/16.
 */
public class AddVehicleDialogFragment extends RoboDialogFragment {

    public static final String TAG = AddVehicleDialogFragment.class.getSimpleName();

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public AddVehicleDialogFragment build(){
            return new AddVehicleDialogFragment();
        }

    }

    @InjectView(R.id.appcompatbutton_addvehicle_addvehicle)
    private AppCompatButton mAddButton;
    @InjectView(R.id.appcompatbutton_addvehicle_cancel)
    private AppCompatButton mCancelButton;
    @InjectView(R.id.textinputedittext_addvehicle_brand)
    private TextInputEditText mBrandTextInput;
    @InjectView(R.id.textinputedittext_addvehicle_plates)
    private TextInputEditText mPlatesTextInput;
    @InjectView(R.id.appcompatcheckbox_addvehicle_acceptschildren)
    private AppCompatCheckBox mAcceptsChildren;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterForNotifications();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialogfragment_addvehicle, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(false);

        mCancelButton.setOnClickListener(mOnClickListener);
        mAddButton.setOnClickListener(mOnClickListener);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AppCompatDialog(getContext());
    }

    private void registerForNotifications(){
        IntentFilter intentFilter = new IntentFilter(VehiclesService.BROADCAST_ACTION_VEHICLE_CREATED);

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getContext());
        broadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void unregisterForNotifications(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void onAddVehicleClick(){
        ProgressDialogFragment.showProgressDialog(getFragmentManager());

        String brand = mBrandTextInput.getText().toString();
        String plates = mPlatesTextInput.getText().toString();
        boolean accepts = mAcceptsChildren.isChecked();

        Intent intent = new Intent(getContext(), VehiclesService.class);
        intent.setAction(VehiclesService.ACTION_CREATE_VEHICLE);
        intent.putExtra(VehiclesService.EXTRA_ACCEPTS, accepts);
        intent.putExtra(VehiclesService.EXTRA_PLATES, plates);
        intent.putExtra(VehiclesService.EXTRA_BRAND, brand);

        getContext().startService(intent);
    }

    private void onCancelClick(){
        dismiss();
    }

    private void processBroadcastResult(Intent intent){
        ProgressDialogFragment.dismissProgressDialog();

        boolean created = intent.getBooleanExtra(VehiclesService.BROADCAST_EXTRA_VEHICLE_CREATED, false);
        if(created){
            Toast.makeText(getContext(), "Vehículo Añadido Exitósamente", Toast.LENGTH_SHORT).show();

            Intent serviceIntent = new Intent(getContext(), VehiclesService.class);
            serviceIntent.setAction(VehiclesService.ACTION_GET_VEHICLES);
            getContext().startService(serviceIntent);

            dismiss();

        }else{
            Toast.makeText(getContext(), "Hubo un error al crear su vehículo", Toast.LENGTH_SHORT).show();
        }
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(VehiclesService.BROADCAST_ACTION_VEHICLE_CREATED.equals(action)){
                processBroadcastResult(intent);
            }
        }
    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.appcompatbutton_addvehicle_addvehicle){
                onAddVehicleClick();
            }else if(viewId == R.id.appcompatbutton_addvehicle_cancel){
                onCancelClick();
            }
        }
    };
}

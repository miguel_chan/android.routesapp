package merida.tec.routesapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import merida.tec.routesapp.R;
import merida.tec.routesapp.models.User;
import merida.tec.routesapp.services.UserService;
import merida.tec.routesapp.utils.StringUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 30/05/16.
 */
public class CreateUserFragment extends RoboFragment {

    public static final String TAG = CreateUserFragment.class.getSimpleName();

    @InjectView(R.id.textinputedittext_createuser_email)
    private TextInputEditText mUsernameEditText;
    @InjectView(R.id.textinputedittext_createuser_password)
    private TextInputEditText mPasswordEditText;
    @InjectView(R.id.textinputedittext_createuser_gender)
    private TextInputEditText mGenderEditText;
    @InjectView(R.id.textinputedittext_createuser_confirmpassword)
    private TextInputEditText mConfirmPasswordEditText;
    @InjectView(R.id.textinputedittext_createuser_phonenumber)
    private TextInputEditText mPhoneNumberEditText;
    @InjectView(R.id.appcompatbutton_createuser_create)
    private AppCompatButton mCreateButton;

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public CreateUserFragment build(){
            return new CreateUserFragment();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterForNotifications();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_createuser, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCreateButton.setOnClickListener(mOnClickListener);
    }

    private void registerForNotifications(){
        IntentFilter intentFilter =
                new IntentFilter(UserService.BROADCAST_ACTION_CREATION);
        LocalBroadcastManager localBroadcastManager =
                LocalBroadcastManager.getInstance(getContext());

        localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void unregisterForNotifications(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void onCreateUserclick(){
        String phoneNumber = mPhoneNumberEditText.getText().toString();
        String userName = mUsernameEditText.getText().toString();
        String gender = mGenderEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();
        String confirmPassword = mConfirmPasswordEditText.getText().toString();

        if(!StringUtils.isValidString(phoneNumber)){
            setError("Se requiere el numero telefónico", mPhoneNumberEditText);
            return;
        }else{
            setError(null, mPhoneNumberEditText);
        }

        if(!StringUtils.isValidString(userName)){
            setError("Se requiere el Correo Electrónico", mUsernameEditText);
            return;
        }else{
            setError(null, mUsernameEditText);
        }

        if (!StringUtils.isValidString(gender)) {
            setError("Se requiere el Género", mGenderEditText);
            return;
        }else{
            setError(null, mGenderEditText);
        }

        if(!StringUtils.isValidString(password)){
            setError("Se requiere la contraseña", mPasswordEditText);
            return;
        }else{
            setError(null, mPasswordEditText);
        }

        if(!StringUtils.isValidString(confirmPassword)){
            setError("Se requiere la confirmación de Contraseña", mConfirmPasswordEditText);
            return;
        }else{
            setError(null, mConfirmPasswordEditText);
        }

        if(!confirmPassword.equals(password)){
            setError("Contraseñas Diferentes", mConfirmPasswordEditText);
            return;
        }else{
            setError(null, mConfirmPasswordEditText);
        }

        ProgressDialogFragment.showProgressDialog(getFragmentManager());

        User user = new User(0, userName, phoneNumber, password, gender);

        Intent intent = new Intent(getContext(), UserService.class);
        intent.putExtra(UserService.EXTRA_USER, user);
        getActivity().startService(intent);
    }

    private void showMessage(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private void processBroadcastResult(Intent intent){
        boolean created =
                intent.getBooleanExtra(UserService.BROADCAST_EXTRA_USER_CREATED, false);

        String message = "Hubo un error al crear el Usuario";
        if(created){
            message = "Usuario Creado con Éxito";
        }

        ProgressDialogFragment.dismissProgressDialog();
        showMessage(message);
        if(created) {
            getFragmentManager().popBackStack();
        }
    }

    private void setError(String error, TextInputEditText editText){
        editText.setError(error);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.appcompatbutton_createuser_create){
                onCreateUserclick();
            }
        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            processBroadcastResult(intent);
        }
    };
}

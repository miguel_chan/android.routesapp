package merida.tec.routesapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.IntentCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import merida.tec.routesapp.R;
import merida.tec.routesapp.activities.MainActivity;
import merida.tec.routesapp.services.UserService;
import merida.tec.routesapp.utils.FragmentHandler;
import merida.tec.routesapp.utils.StringUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 30/05/16.
 */
public class LogInFragment extends RoboFragment{

    public static final String TAG = LogInFragment.class.getSimpleName();

    private FragmentHandler mFragmentHandler;

    @InjectView(R.id.appcompatbutton_login_login)
    private AppCompatButton mLogInButton;
    @InjectView(R.id.appcompatbutton_login_createuser)
    private AppCompatButton mCreateUserButton;
    @InjectView(R.id.textinputedittext_login_username)
    private TextInputEditText mUserNameEditText;
    @InjectView(R.id.textinputedittext_login_password)
    private TextInputEditText mPasswordEditText;

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public LogInFragment build(){
            return new LogInFragment();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterForNotifications();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getActivity() instanceof FragmentHandler){
            mFragmentHandler = (FragmentHandler) getActivity();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLogInButton.setOnClickListener(mOnClickListener);
        mCreateUserButton.setOnClickListener(mOnClickListener);
    }

    private void registerForNotifications(){
        IntentFilter intentFilter = new IntentFilter(UserService.BROADCAST_ACTION_LOG_IN);

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void unregisterForNotifications(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void setError(String error, TextInputEditText editText){
        editText.setError(error);
    }

    private void onLogInClick(){
        String userName = mUserNameEditText.getText().toString();
        if(!StringUtils.isValidString(userName)){
            setError("Nombre de Usuario requerido", mUserNameEditText);
            return;
        }else{
            setError(null, mUserNameEditText);
        }

        String password = mPasswordEditText.getText().toString();
        if(!StringUtils.isValidString(password)){
            setError("Contraseña Requerida", mPasswordEditText);
            return;
        }else{
            setError(null, mPasswordEditText);
        }

        Intent intent = new Intent(getContext(), UserService.class);
        intent.setAction(UserService.ACTION_LOG_IN);
        intent.putExtra(UserService.EXTRA_USER_NAME, userName);
        intent.putExtra(UserService.EXTRA_PASSWORD, password);

        ProgressDialogFragment.showProgressDialog(getFragmentManager());

        getContext().startService(intent);
    }

    private void startMainActivity(){
        Intent intent = MainActivity.Builder.newBuilder().build(getContext());
        intent = IntentCompat.makeRestartActivityTask(intent.getComponent());
        getActivity().startActivity(intent);
    }

    private void processLogInResult(Intent intent){
        boolean loggedIn = intent.getBooleanExtra(UserService.BROADCAST_EXTRA_LOGGED_IN, false);

        ProgressDialogFragment.dismissProgressDialog();
        if(loggedIn){
            showMessage("Inicio de Sessión Correcto");
            startMainActivity();
        }else{
            showMessage("Nombre de Usuario ó Contraseña Inválidos");
        }

    }

    private void showMessage(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private void onCreateUserClick(){
        CreateUserFragment fragment = CreateUserFragment.Builder.newBuilder().build();
        mFragmentHandler.addFragment(fragment, CreateUserFragment.TAG, true);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(UserService.BROADCAST_ACTION_LOG_IN.equals(action)){
                processLogInResult(intent);
            }
        }
    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.appcompatbutton_login_login){
                onLogInClick();
            }else if(viewId == R.id.appcompatbutton_login_createuser){
                onCreateUserClick();
            }
        }
    };
}

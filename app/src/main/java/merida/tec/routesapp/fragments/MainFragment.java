package merida.tec.routesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import merida.tec.routesapp.R;
import merida.tec.routesapp.utils.FragmentHandler;
import merida.tec.routesapp.utils.ObjectUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 31/05/16.
 */
public class MainFragment extends RoboFragment {

    public static final String TAG = MainFragment.class.getSimpleName();

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public MainFragment build(){
            MainFragment mainFragment = new MainFragment();
            return  mainFragment;
        }

    }

    private FragmentHandler mFragmentHandler;

    @InjectView(R.id.appcompatbutton_main_viewusers)
    private AppCompatButton mViewUsersButton;
    @InjectView(R.id.appcompatbutton_main_viewroutes)
    private AppCompatButton mViewRoutesButton;
    @InjectView(R.id.appcompatbutton_main_viewvehicles)
    private AppCompatButton mViewVehiclesButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getActivity() instanceof FragmentHandler){
            mFragmentHandler = (FragmentHandler) getActivity();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        configureClickListeners();
    }

    private void loadVehiclesFragment(){
        VehiclesFragment vehiclesFragment = VehiclesFragment
                .Builder
                .newBuilder()
                .build();
        mFragmentHandler.addFragment(vehiclesFragment, VehiclesFragment.TAG, true);
    }

    private void loadUsersFragment(){
        UsersFragment usersFragment = UsersFragment
                .Builder
                .newBuilder()
                .build();
        mFragmentHandler.addFragment(usersFragment, UsersFragment.TAG, true);
    }

    private void configureClickListeners(){
        mViewRoutesButton.setOnClickListener(mOnClickListener);
        mViewUsersButton.setOnClickListener(mOnClickListener);
        mViewVehiclesButton.setOnClickListener(mOnClickListener);
    }

    private void loadRoutesFragment(){
        RoutesFragment routesFragment = RoutesFragment
                .Builder
                .newBuilder()
                .build();
        mFragmentHandler.addFragment(routesFragment, RoutesFragment.TAG, true);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.appcompatbutton_main_viewroutes){
                loadRoutesFragment();
            }else if(viewId == R.id.appcompatbutton_main_viewusers){
                loadUsersFragment();
            }else if(viewId == R.id.appcompatbutton_main_viewvehicles){
                loadVehiclesFragment();
            }
        }
    };
}

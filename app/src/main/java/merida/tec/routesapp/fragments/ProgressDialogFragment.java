package merida.tec.routesapp.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import merida.tec.routesapp.R;
import roboguice.fragment.RoboDialogFragment;

/**
 * Created by miguelchan on 10/02/16.
 */
public class ProgressDialogFragment extends RoboDialogFragment {

    public static final String TAG = ProgressDialogFragment.class.getSimpleName();

    public static void showProgressDialog(FragmentManager fragmentManager){
        if(INSTANCE == null){
            INSTANCE = new ProgressDialogFragment();
            INSTANCE.show(fragmentManager, TAG);
        }
    }

    public static void dismissProgressDialog(){
        if(INSTANCE != null){
            try {
                INSTANCE.dismiss();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        INSTANCE = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setCancelable(false);
        return inflater.inflate(R.layout.dialogfragment_progress, container, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog aDialog = new Dialog(getActivity(),android.R.style.Theme_Black_NoTitleBar);
        return aDialog;
    }

    private static ProgressDialogFragment INSTANCE;
}

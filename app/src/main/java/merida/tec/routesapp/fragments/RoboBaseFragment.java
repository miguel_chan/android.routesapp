package merida.tec.routesapp.fragments;

import android.content.Context;

import merida.tec.routesapp.utils.FragmentHandler;
import roboguice.fragment.RoboFragment;

/**
 * Created by miguelchan on 31/05/16.
 */
public class RoboBaseFragment extends RoboFragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getActivity() instanceof FragmentHandler){
            mFragmentHandler = (FragmentHandler) getActivity();
        }
    }

    protected FragmentHandler mFragmentHandler;
}

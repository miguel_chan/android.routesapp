package merida.tec.routesapp.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;
import com.google.appengine.repackaged.com.google.common.base.Function;
import com.google.appengine.repackaged.com.google.common.collect.FluentIterable;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.PendingResult;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.EncodedPolyline;
import com.google.maps.model.GeocodingResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import merida.tec.routesapp.R;
import merida.tec.routesapp.models.Coordinate;
import merida.tec.routesapp.models.Route;
import merida.tec.routesapp.services.RoutesService;
import merida.tec.routesapp.utils.ListUtils;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.utils.PolylineUtils;
import merida.tec.routesapp.utils.StringUtils;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 01/06/16.
 */
public class RouteMapFragment extends RoboBaseFragment {

    public static final String TAG = RouteMapFragment.class.getSimpleName();

    public static class Builder {

        private Route route;

        public static Builder newBuilder(){
            return new Builder();
        }

        public Builder setRoute(Route route){
            this.route = route;
            return this;
        }

        public RouteMapFragment build(){
            Bundle args = new Bundle();

            if(ObjectUtils.isNotNull(route)) {
                args.putParcelable(EXTRA_ROUTE, route);
            }

            RouteMapFragment fragment = new RouteMapFragment();
            fragment.setArguments(args);
            return fragment;
        }

    }

    private static final String API_KEY = "AIzaSyDYcaK92WQXnT01ig-0qlsVAWhe_f_EYw0";

    private static final String MAP_TAG = "GoogleMap";
    private static final String EXTRA_START_MARKER = "ExtraStartMarker";
    private static final String EXTRA_END_MARKER = "ExtraEndMarker";
    private static final String EXTRA_CURRENT_MODE = "ExtraCurrentMode";
    private static final String EXTRA_POLYLINES = "ExtraPolylines";
    private static final String EXTRA_SELECTED_POLYLINE = "ExtraSelectedPolyline";
    private static final String EXTRA_START_TEXT = "ExtraStartText";
    private static final String EXTRA_END_TEXT = "ExtraEndText";
    private static final String EXTRA_ROUTE_NAME = "ExtraRouteName";
    private static final String EXTRA_ROUTE = "ExtraRoue";

    private static final int MODE_INITIAL_POINT = 2;
    private static final int MODE_END_POINT = 3;

    private static final int REQUEST_CODE_PERMISSIONS = 1;
    private static final int NORMAL_INTERVAL = 5 * 1000 * 60;
    private static final int FASTEST_INTERVAL = 10 * 1000;

    private BottomSheetBehavior mBottomSheetBehavior;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private boolean mIsConnected;

    private Marker mCurrentLocation;
    private Marker mGStartMarker;
    private Marker mGEndMarker;
    private ArrayList<PolylineOptions> mPolylinesOptions = Lists.newArrayList();
    private ArrayList<Polyline> mPolylines = Lists.newArrayList();
    private HashMap<Polyline, PolylineOptions> mPolylineMap;
    private Polyline mPreviousPolyline;

    private int mCurrentMode = MODE_INITIAL_POINT;
    private MarkerOptions mStartMarker;
    private MarkerOptions mEndMarker;
    private PolylineOptions mSelectedPolyline;
    private String mStartText;
    private String mEndText;
    private String mRouteName;

    @InjectView(R.id.nestedscrollview_routemap_bottomsheet)
    private NestedScrollView mBottomSheetContainer;
    @InjectView(R.id.appcompatbutton_route_setstartpoint)
    private AppCompatButton mSetInitialPointButton;
    @InjectView(R.id.appcompatbutton_route_setendpoint)
    private AppCompatButton mSetEndPointButton;
    @InjectView(R.id.appcompattextview_route_title)
    private AppCompatTextView mBottomSheetTitle;
    @InjectView(R.id.textinputedittext_route_routename)
    private TextInputEditText mRouteNameEditText;
    @InjectView(R.id.appcompatbutton_route_createroute)
    private AppCompatButton mAddRouteButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        restoreInstance(savedInstanceState);
        return inflater.inflate(R.layout.fragment_routemap, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        configureBottomSheet();

        mSetEndPointButton.setOnClickListener(mOnClickListener);
        mSetInitialPointButton.setOnClickListener(mOnClickListener);
        mBottomSheetTitle.setOnClickListener(mOnClickListener);
        mAddRouteButton.setOnClickListener(mOnClickListener);

        if(StringUtils.isValidString(mStartText)){
            mSetInitialPointButton.setText(mStartText);
        }
        if(StringUtils.isValidString(mEndText)){
            mSetEndPointButton.setText(mEndText);
        }

        if(ObjectUtils.isNull(savedInstanceState)){
            addMapFragment();
        }else{
            requestPermissions();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
            configureGoogleMaps();
        }else{
            Toast.makeText(
                    getContext(),
                    "Se requiere permiso de Ubicación para Utilizar esta Sección",
                    Toast.LENGTH_SHORT
            ).show();
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(ObjectUtils.isNotNull(mStartMarker)){
            outState.putParcelable(EXTRA_START_MARKER, mStartMarker);
        }
        if(ObjectUtils.isNotNull(mEndMarker)){
            outState.putParcelable(EXTRA_END_MARKER, mEndMarker);
        }
        if(ListUtils.isValidList(mPolylines)){
            outState.putParcelableArrayList(EXTRA_POLYLINES, mPolylinesOptions);
        }
        if(ObjectUtils.isNotNull(mSelectedPolyline)){
            outState.putParcelable(EXTRA_SELECTED_POLYLINE, mSelectedPolyline);
        }
        mStartText = mSetInitialPointButton.getText().toString();
        outState.putString(EXTRA_START_TEXT, mStartText);

        mEndText = mSetEndPointButton.getText().toString();
        outState.putString(EXTRA_END_TEXT, mEndText);

        outState.putInt(EXTRA_CURRENT_MODE, mCurrentMode);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterForNotifications();

        if(mIsConnected && ObjectUtils.isNotNull(mGoogleApiClient)){
            mGoogleApiClient.disconnect();
            mIsConnected = false;
        }
        mGoogleApiClient = null;
        mCurrentLocation = null;

        if(ObjectUtils.isNotNull(mGEndMarker)){
            mGEndMarker.remove();
            mGEndMarker = null;
        }
        if(ObjectUtils.isNotNull(mGStartMarker)){
            mGStartMarker.remove();
            mGStartMarker = null;
        }
        if(ObjectUtils.isNotNull(mCurrentLocation)){
            mCurrentLocation.remove();
            mCurrentLocation = null;
        }

        removePolylines(false);
    }

    private void registerForNotifications(){
        IntentFilter intentFilter = new IntentFilter(RoutesService.BROADCAST_ACTION_CREATE_ROUTE);

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getContext());
        broadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void unregisterForNotifications(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void configureBottomSheet(){
        int peekSize = getResources().getDimensionPixelSize(R.dimen.bottom_sheet_peeksize);
        mBottomSheetBehavior = BottomSheetBehavior.from(mBottomSheetContainer);
        mBottomSheetBehavior.setPeekHeight(peekSize);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setBottomSheetCallback(mBottomSheetCallback);
    }

    private void loadArguments(){
        Bundle arguments = getArguments();
        if(ObjectUtils.isNotNull(arguments) && arguments.containsKey(EXTRA_ROUTE)){
            Route aRoute = arguments.getParcelable(EXTRA_ROUTE);
            Coordinate startCoordinate = aRoute.getStartLocation();
            Coordinate endCoordinate = aRoute.getEndLocation();
            String polyline = aRoute.getEncodedPolyline();
            String routeName = aRoute.getRouteName();

            mStartMarker = new MarkerOptions();
            mStartMarker.title("Ubicación de Inicio");
            mStartMarker.position(new LatLng(startCoordinate.getLatitude(), startCoordinate.getLongitude()));
            requestReverseGeocoding(mStartMarker.getPosition(), mSetInitialPointButton);

            mEndMarker = new MarkerOptions();
            mEndMarker.title("Ubicación Final");
            mEndMarker.position(new LatLng(endCoordinate.getLatitude(), endCoordinate.getLongitude()));
            requestReverseGeocoding(mEndMarker.getPosition(), mSetEndPointButton);

            mSelectedPolyline = new PolylineOptions();
            mSelectedPolyline.color(Color.BLACK);
            mSelectedPolyline.addAll(PolylineUtils.toLatLng(polyline));

            mRouteNameEditText.setText(routeName);

            mGStartMarker = mGoogleMap.addMarker(mStartMarker);
            mGEndMarker = mGoogleMap.addMarker(mEndMarker);
            mGoogleMap.addPolyline(mSelectedPolyline);

            mAddRouteButton.setVisibility(View.GONE);
            mSetEndPointButton.setClickable(false);
            mSetInitialPointButton.setClickable(false);
            mRouteNameEditText.setClickable(false);
        }
    }

    private boolean canEdit(){
        Bundle args = getArguments();
        if(ObjectUtils.isNotNull(args) && args.containsKey(EXTRA_ROUTE)){
            return false;
        }
        return true;
    }

    private void restoreInstance(Bundle bundle){
        if(ObjectUtils.isNotNull(bundle)){
            mStartMarker = bundle.getParcelable(EXTRA_START_MARKER);
            mEndMarker = bundle.getParcelable(EXTRA_END_MARKER);
            mCurrentMode = bundle.getInt(EXTRA_CURRENT_MODE, MODE_INITIAL_POINT);
            mPolylinesOptions = bundle.getParcelableArrayList(EXTRA_POLYLINES);
            mSelectedPolyline = bundle.getParcelable(EXTRA_SELECTED_POLYLINE);
            mStartText = bundle.getString(EXTRA_START_TEXT, null);
            mEndText = bundle.getString(EXTRA_END_TEXT, null);
        }
    }

    private void requestPermissions(){
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            final String[] permission = {
                    Manifest.permission.ACCESS_FINE_LOCATION
            };
            requestPermissions(permission, REQUEST_CODE_PERMISSIONS);
        }else {
            configureGoogleMaps();
        }
    }

    private void drawRoutes(DirectionsRoute[] routes){
        mPolylineMap = new HashMap<>();
        for(int i=0;i<routes.length;i++){
            DirectionsRoute currentRoute = routes[i];
            EncodedPolyline encodedPolyline = currentRoute.overviewPolyline;

            PolylineOptions options = new PolylineOptions();
            List<LatLng> points = convert(encodedPolyline.decodePath());
            options.addAll(points);
            options.color(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));


            Polyline polyline = mGoogleMap.addPolyline(options);
            mPolylineMap.put(polyline, options);

            mPolylinesOptions.add(options);
            mPolylines.add(polyline);

            if(ObjectUtils.isNotNull(mSelectedPolyline) &&
                    PolylineUtils.equals(mSelectedPolyline, options)){
                polyline.setColor(Color.BLACK);
                polyline.setClickable(false);
                polyline.setZIndex(10);
                mPreviousPolyline = polyline;
            }else if(i == 0) {
                polyline.setColor(Color.BLACK);
                polyline.setClickable(false);
                polyline.setZIndex(10);
                mPreviousPolyline = polyline;
                mSelectedPolyline = options;
            }else {
                polyline.setClickable(true);
                polyline.setZIndex(1);
                polyline.setColor(getResources().getColor(R.color.colorPrimary));
            }
        }
    }

    private void removePolylines(boolean clearOptions){
        for(Polyline currentPolyline : mPolylines){
            currentPolyline.remove();
        }
        if(ObjectUtils.isNotNull(mPolylines)) {
            mPolylines.clear();
        }
        if(ObjectUtils.isNotNull(mPolylineMap)) {
            mPolylineMap.clear();
        }

        if(clearOptions) {
            mPolylinesOptions.clear();
        }
    }

    private void mayGetDirections(){
        if(ObjectUtils.isNotNull(mStartMarker) && ObjectUtils.isNotNull(mEndMarker)) {
            GeoApiContext context = new GeoApiContext();
            context.setApiKey(API_KEY);
            String startValues = mStartMarker.getPosition().latitude + "," + mStartMarker.getPosition().longitude;
            String endValues = mEndMarker.getPosition().latitude + "," + mEndMarker.getPosition().longitude;

            DirectionsApiRequest request =
                    DirectionsApi.getDirections(context, startValues, endValues);

            request.alternatives(true);
            request.setCallback(new PendingResult.Callback<DirectionsResult>() {
                @Override
                public void onResult(final DirectionsResult result) {
                    getView().post(new Runnable() {
                        @Override
                        public void run() {
                            removePolylines(true);
                            drawRoutes(result.routes);
                        }
                    });
                }

                @Override
                public void onFailure(Throwable e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private List<LatLng> convert(List<com.google.maps.model.LatLng> points){
        return FluentIterable.from(points).transform(new Function<com.google.maps.model.LatLng, LatLng>() {
            @Override
            public LatLng apply(com.google.maps.model.LatLng latLng) {
                return new LatLng(latLng.lat, latLng.lng);
            }
        }).toList();
    };

    private void configureGoogleMaps(){
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentByTag(MAP_TAG);
        if(ObjectUtils.isNotNull(mapFragment)){
            mapFragment.getMapAsync(mOnMapReadyCallback);
        }
    }

    private void addMapFragment(){
        GoogleMapOptions options = new GoogleMapOptions();
        options.mapToolbarEnabled(false);
        options.zoomGesturesEnabled(true);
        options.compassEnabled(true);

        SupportMapFragment mapFragment = SupportMapFragment.newInstance(options);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.framelayout_routemap_mapcontainer, mapFragment, MAP_TAG);
        fragmentTransaction.commit();

        getChildFragmentManager().executePendingTransactions();

        requestPermissions();
    }

    private void configureGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient
                .Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(mConnectionCallbacks)
                .build();
        mGoogleApiClient.connect();
    }

    private void requestUserLocation(){
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        locationRequest.setInterval(NORMAL_INTERVAL);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                locationRequest,
                mLocationListener
        );
    }

    private void updateCurrentUserLocation(Location location){
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if(ObjectUtils.isNull(mCurrentLocation)) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.snippet("Mi Ubicación");
            markerOptions.title("Mi Ubicación");
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher));

            mCurrentLocation = mGoogleMap.addMarker(markerOptions);

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mGoogleMap.moveCamera(cameraUpdate);
        }else{
            mCurrentLocation.setPosition(latLng);
        }
    }

    private void onCreateRouteClick(){
        if(ObjectUtils.isNull(mStartMarker)){
            Toast.makeText(getContext(), "Aún no ha seleccionado un punto de Inicio", Toast.LENGTH_SHORT).show();
            return;
        }
        if(ObjectUtils.isNull(mEndMarker)){
            Toast.makeText(getContext(), "Aúnno ha seleccionado un punto Final", Toast.LENGTH_SHORT).show();
            return;
        }
        if(ObjectUtils.isNull(mSelectedPolyline)){
            Toast.makeText(getContext(), "Aún no se ha seleccionado una Ruta", Toast.LENGTH_SHORT).show();
            return;
        }

        String routeName = mRouteNameEditText.getText().toString();
        if(!StringUtils.isValidString(routeName)){
            mRouteNameEditText.setError("Campo Requerido");
            return;
        }else{
            mRouteNameEditText.setError(null);
        }

        Coordinate startCoordinate = new Coordinate(
                mStartMarker.getPosition().latitude,
                mStartMarker.getPosition().longitude
        );
        Coordinate endCoordinate = new Coordinate(
                mEndMarker.getPosition().latitude,
                mEndMarker.getPosition().longitude
        );

        ProgressDialogFragment.showProgressDialog(getFragmentManager());

        String encodedPolyline = PolylineUtils.encodePolyline(mSelectedPolyline);
        RoutesService
                .Builder
                .createRoute(getContext())
                .setCoordinates(startCoordinate, endCoordinate)
                .setPolyline(encodedPolyline)
                .setRouteName(routeName)
                .buildAndStart();
    }

    private void processRouteCreationResult(Intent intent){
        ProgressDialogFragment.dismissProgressDialog();

        boolean routeCreated = intent.getBooleanExtra(RoutesService.BROADCAST_EXTRA_ROUTE_CREATED, false);
        if(routeCreated){
            Toast.makeText(getContext(), "La ruta se ha creado con éxito", Toast.LENGTH_LONG).show();
            getFragmentManager().popBackStack();
        }else{
            Toast.makeText(getContext(), "Hubo un error al Crear la Ruta", Toast.LENGTH_LONG).show();
        }
    }

    private void setGeocodingResult(String value, AppCompatButton button){
        button.setText(value);
    }

    private void processStartPoint(LatLng latLng){
        mStartMarker = createMarkerOptions("Punto Inicial", latLng);

        if(ObjectUtils.isNull(mGStartMarker)) {
            mGStartMarker = mGoogleMap.addMarker(mStartMarker);
        }else{
            mGStartMarker.setPosition(latLng);
        }

        mCurrentMode = MODE_END_POINT;
        requestReverseGeocoding(latLng, mSetInitialPointButton);
    }

    private void processEndPoint(LatLng latLng){
        mEndMarker = createMarkerOptions("Punto Final", latLng);

        if(ObjectUtils.isNull(mGEndMarker)) {
            mGEndMarker = mGoogleMap.addMarker(mEndMarker);
        }else{
            mGEndMarker.setPosition(latLng);
        }

        mCurrentMode = MODE_INITIAL_POINT;
        requestReverseGeocoding(latLng, mSetEndPointButton);
    }

    private void requestReverseGeocoding(LatLng latLng, final AppCompatButton button){
        GeoApiContext context = new GeoApiContext();
        context.setApiKey(API_KEY);

        GeocodingApiRequest request = GeocodingApi.newRequest(context);
        request.latlng(new com.google.maps.model.LatLng(latLng.latitude, latLng.longitude));
        request.setCallback(new PendingResult.Callback<GeocodingResult[]>() {
            @Override
            public void onResult(final GeocodingResult[] result) {
                if(result.length > 0){
                    getView().post(new Runnable() {
                        @Override
                        public void run() {
                            String formattedAddress = result[0].formattedAddress;
                            setGeocodingResult(formattedAddress, button);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Throwable e) {
                e.printStackTrace();
            }
        });
    }

    private void drawPolylines(List<PolylineOptions> optionses){
        mPolylineMap = new HashMap();

        for(PolylineOptions currentOption : optionses){
            Polyline polyline = mGoogleMap.addPolyline(currentOption);
            mPolylines.add(polyline);
            mPolylineMap.put(polyline, currentOption);
            if(ObjectUtils.isNotNull(mSelectedPolyline) &&
                    PolylineUtils.equals(mSelectedPolyline, currentOption)){
                polyline.setColor(Color.BLACK);
                polyline.setClickable(false);
                polyline.setZIndex(10);
                mPreviousPolyline = polyline;
            }else{
                polyline.setColor(getResources().getColor(R.color.colorPrimary));
                polyline.setClickable(true);
                polyline.setZIndex(1);
            }
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.appcompatbutton_route_setstartpoint){
                mCurrentMode = MODE_INITIAL_POINT;
            }else if(viewId == R.id.appcompatbutton_route_setendpoint){
                mCurrentMode = MODE_END_POINT;
            }else if(viewId == R.id.appcompattextview_route_title){
                int state = mBottomSheetBehavior.getState();
                if(state == BottomSheetBehavior.STATE_COLLAPSED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }else{
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }else if(viewId == R.id.appcompatbutton_route_createroute){
                onCreateRouteClick();
            }
        }
    };

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            updateCurrentUserLocation(location);
        }
    };

    private GoogleApiClient.ConnectionCallbacks mConnectionCallbacks =
            new GoogleApiClient.ConnectionCallbacks() {

        @Override
        public void onConnected(@Nullable Bundle bundle) {
            mIsConnected = true;
            requestUserLocation();
        }

        @Override
        public void onConnectionSuspended(int i) {

        }
    };

    private OnMapReadyCallback mOnMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mGoogleMap = googleMap;

            configureGoogleApiClient();

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.setOnMapLongClickListener(mOnMapLongClickListener);
            mGoogleMap.setOnPolylineClickListener(mOnPolylineClickListener);

            if(ObjectUtils.isNotNull(mStartMarker)){
                mGoogleMap.addMarker(mStartMarker);
            }
            if(ObjectUtils.isNotNull(mEndMarker)){
                mGoogleMap.addMarker(mEndMarker);
            }

            if(ObjectUtils.isNotNull(mPolylinesOptions)){
                drawPolylines(mPolylinesOptions);
            }

            loadArguments();
        }
    };

    private MarkerOptions createMarkerOptions(String title, LatLng latLng){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.title(title);
        markerOptions.snippet(title);
        markerOptions.position(latLng);
        return markerOptions;
    }

    private GoogleMap.OnMapLongClickListener mOnMapLongClickListener =
            new GoogleMap.OnMapLongClickListener() {

        @Override
        public void onMapLongClick(LatLng latLng) {
            if(!canEdit()){
                return;
            }

            if(mCurrentMode == MODE_INITIAL_POINT){
                processStartPoint(latLng);
            }else if(mCurrentMode == MODE_END_POINT){
                processEndPoint(latLng);
            }

            mayGetDirections();
        }
    };

    private GoogleMap.OnPolylineClickListener mOnPolylineClickListener =
            new GoogleMap.OnPolylineClickListener() {

        @Override
        public void onPolylineClick(Polyline polyline) {
            if(ObjectUtils.isNotNull(mPreviousPolyline)){
                mPreviousPolyline.setClickable(true);
                mPreviousPolyline.setColor(getResources().getColor(R.color.colorPrimary));
                mPreviousPolyline.setZIndex(1);
            }
            polyline.setClickable(false);
            polyline.setColor(Color.BLACK);
            polyline.setZIndex(10);
            mPreviousPolyline = polyline;

            mSelectedPolyline = mPolylineMap.get(polyline);
        }

    };

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if(newState == BottomSheetBehavior.STATE_COLLAPSED){
                int padding = getResources().getDimensionPixelSize(R.dimen.bottom_sheet_peeksize);
                mGoogleMap.setPadding(0, 0, 0, padding);
            }else if(newState == BottomSheetBehavior.STATE_EXPANDED){
                int measuredHeight = bottomSheet.getMeasuredHeight();
                mGoogleMap.setPadding(0, 0, 0, measuredHeight);
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(RoutesService.BROADCAST_ACTION_CREATE_ROUTE.equals(action)){
                processRouteCreationResult(intent);
            }
        }
    };

}

package merida.tec.routesapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import merida.tec.routesapp.R;
import merida.tec.routesapp.adapters.RoutesAdapter;
import merida.tec.routesapp.models.Route;
import merida.tec.routesapp.services.RoutesService;
import merida.tec.routesapp.utils.SimpleDividerItemDecoration;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 01/06/16.
 */
public class RoutesFragment extends RoboBaseFragment {

    public static final String TAG = RoutesFragment.class.getSimpleName();

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public RoutesFragment build(){
            return new RoutesFragment();
        }

    }

    @InjectView(R.id.recyclerview_recycler)
    private RecyclerView mRecyclerView;
    @InjectView(R.id.floatingactionbutton_floatingactionbutton)
    private FloatingActionButton mFloatingActionButton;
    @InjectView(R.id.swiperefreshlayout_swiperefresh)
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterForNotifications();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFloatingActionButton.setOnClickListener(mOnClickListener);
        mFloatingActionButton.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        requestRoutes();
    }

    private void setAdapter(List<Route> routes){
        RoutesAdapter adapter = new RoutesAdapter(routes);
        adapter.setOnRouteClickListener(mOnRouteClickListener);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
    }

    private void registerForNotifications(){
        IntentFilter intentFilter = new IntentFilter(RoutesService.BROADCAST_ACTION_CREATE_ROUTE);
        intentFilter.addAction(RoutesService.BROADCAST_ACTION_FETCH_ROUTES);

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getContext());
        broadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void processRoutesResult(Intent intent){
        mSwipeRefreshLayout.setRefreshing(false);
        List<Route> routes = intent.getParcelableArrayListExtra(RoutesService.BROADCAST_EXTRA_ROUTES);
        setAdapter(routes);
    }

    private void unregisterForNotifications(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void requestRoutes(){
        RoutesService
                .Builder
                .fetchRoutes(getContext())
                .buildAndStart();
    }

    private void loadMapsFragment(Route route){
        RouteMapFragment routeMapFragment = RouteMapFragment
                .Builder
                .newBuilder()
                .setRoute(route)
                .build();
        mFragmentHandler.addFragment(routeMapFragment, RouteMapFragment.TAG, true);
    }

    private void onAddRouteClick(){
        RouteMapFragment routeMapFragment = RouteMapFragment
                .Builder
                .newBuilder()
                .build();
        mFragmentHandler.addFragment(routeMapFragment, RouteMapFragment.TAG, true);
    }

    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener =
            new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            requestRoutes();
        }

    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.floatingactionbutton_floatingactionbutton){
                onAddRouteClick();
            }
        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(RoutesService.BROADCAST_ACTION_CREATE_ROUTE.equals(action)){
                requestRoutes();
            }else if(RoutesService.BROADCAST_ACTION_FETCH_ROUTES.equals(action)){
                processRoutesResult(intent);
            }
        }
    };

    private RoutesAdapter.OnRouteClickListener mOnRouteClickListener =
            new RoutesAdapter.OnRouteClickListener() {

        @Override
        public void onRouteClick(Route route, View view, int position) {
            loadMapsFragment(route);
        }

    };
}

package merida.tec.routesapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import merida.tec.routesapp.R;
import merida.tec.routesapp.adapters.UsersAdapter;
import merida.tec.routesapp.models.User;
import merida.tec.routesapp.services.UserService;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.utils.SimpleDividerItemDecoration;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 31/05/16.
 */
public class UsersFragment extends RoboFragment {

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public UsersFragment build(){
            UsersFragment usersFragment = new UsersFragment();
            return usersFragment;
        }

    }

    public static final String TAG = UsersFragment.class.getSimpleName();

    @InjectView(R.id.recyclerview_recycler)
    private RecyclerView mRecyclerView;
    @InjectView(R.id.swiperefreshlayout_swiperefresh)
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterForNotifications();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        if(ObjectUtils.isNull(savedInstanceState)){
            loadUsers();
        }
    }

    private void registerForNotifications(){
        IntentFilter intentFilter = new IntentFilter(UserService.BROADCAST_ACTION_GET_USERS);

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);

    }

    private void processUsersResult(Intent intent){
        List<User> users = intent.getParcelableArrayListExtra(UserService.BROADCAST_EXTRA_USERS);

        UsersAdapter adapter = new UsersAdapter(users);

        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(mCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);

        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void unregisterForNotifications(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void loadUsers(){
        mSwipeRefreshLayout.setRefreshing(true);
        Intent intent = new Intent(getContext(), UserService.class);
        intent.setAction(UserService.ACTION_GET_USERS);
        getContext().startService(intent);
    }

    private void reinsertUser(int position){
        UsersAdapter usersAdapter = (UsersAdapter) mRecyclerView.getAdapter();
        if(ObjectUtils.isNotNull(usersAdapter)){
            User anUser = usersAdapter.deleteUser(position);
            usersAdapter.addUser(position, anUser);
        }
    }

    private void deleteUser(int position){
        UsersAdapter usersAdapter = (UsersAdapter) mRecyclerView.getAdapter();
        if(ObjectUtils.isNotNull(usersAdapter)){
            User anUser = usersAdapter.deleteUser(position);
            //ToDo - Delete on Server
        }
    }

    private void mayDeleteObject(final int position){
        AlertDialog alertDialog = new AlertDialog
                .Builder(getContext())
                .setTitle("Eliminar Usuario")
                .setMessage("¿Desea eliminar el usuario seleccionado?")
                .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteUser(position);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        reinsertUser(position);
                    }
                })
                .create();
        alertDialog.show();
    }

    private ItemTouchHelper.SimpleCallback mCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView,
                              RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            mayDeleteObject(position);
        }
    };

    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener =
            new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            loadUsers();
        }

    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(UserService.BROADCAST_ACTION_GET_USERS.equals(action)){
                processUsersResult(intent);
            }
        }
    };
}

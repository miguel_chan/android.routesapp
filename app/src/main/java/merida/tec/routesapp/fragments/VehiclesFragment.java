package merida.tec.routesapp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import merida.tec.routesapp.R;
import merida.tec.routesapp.adapters.VehiclesAdapter;
import merida.tec.routesapp.models.Vehicle;
import merida.tec.routesapp.services.VehiclesService;
import merida.tec.routesapp.utils.ObjectUtils;
import merida.tec.routesapp.utils.SimpleDividerItemDecoration;
import roboguice.inject.InjectView;

/**
 * Created by miguelchan on 31/05/16.
 */
public class VehiclesFragment extends RoboBaseFragment {

    public static class Builder {

        public static Builder newBuilder(){
            return new Builder();
        }

        public VehiclesFragment build(){
            return new VehiclesFragment();
        }

    }

    public static final String TAG = VehiclesFragment.class.getSimpleName();

    @InjectView(R.id.recyclerview_recycler)
    private RecyclerView mRecyclerView;
    @InjectView(R.id.swiperefreshlayout_swiperefresh)
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @InjectView(R.id.floatingactionbutton_floatingactionbutton)
    private FloatingActionButton mFloatingActionButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForNotifications();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterForNotifications();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        mFloatingActionButton.setVisibility(View.VISIBLE);
        mFloatingActionButton.setOnClickListener(mOnClickListener);

        if(ObjectUtils.isNull(savedInstanceState)){
            loadVehicles();
        }
    }

    private void processVehiclesResult(Intent intent){
        List<Vehicle> vehicles =
                intent.getParcelableArrayListExtra(VehiclesService.BROADCAST_EXTRA_VEHICLES);
        setAdapter(vehicles);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void setAdapter(List<Vehicle> vehicles){
        VehiclesAdapter adapter = new VehiclesAdapter(vehicles);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
    }

    private void loadVehicles(){
        mSwipeRefreshLayout.setRefreshing(true);

        Intent intent = new Intent(getContext(), VehiclesService.class);
        intent.setAction(VehiclesService.ACTION_GET_VEHICLES);

        getContext().startService(intent);
    }

    private void registerForNotifications(){
        IntentFilter intentFilter = new IntentFilter(VehiclesService.BROADCAST_ACTION_GET_VEHICLES);

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getContext());
        broadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void onAddVehiclesClick(){
        AddVehicleDialogFragment addVehicleDialogFragment = AddVehicleDialogFragment
                .Builder
                .newBuilder()
                .build();
        addVehicleDialogFragment.show(getFragmentManager(), AddVehicleDialogFragment.TAG);
    }

    private void unregisterForNotifications(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.floatingactionbutton_floatingactionbutton){
                onAddVehiclesClick();
            }
        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(VehiclesService.BROADCAST_ACTION_GET_VEHICLES.equals(action)){
                processVehiclesResult(intent);
            }
        }
    };

    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener =
            new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadVehicles();
        }
    };
}

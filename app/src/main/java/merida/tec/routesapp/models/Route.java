package merida.tec.routesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by miguelchan on 02/06/16.
 */
public class Route implements Parcelable{

    public static final String ID = "Id";
    public static final String ENCODED_POLYLINE = "EncodedPolyline";
    public static final String START_LOCATION = "StartLocation";
    public static final String END_LOCATION = "EndLocation";
    public static final String ROUTE_NAME = "Name";

    public Route() {
    }

    public Route(int id,
                 String encodedPolyline,
                 Coordinate startLocation,
                 Coordinate endLocation,
                 String name) {
        this.id = id;
        this.encodedPolyline = encodedPolyline;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.name = name;
    }

    protected Route(Parcel in) {
        id = in.readInt();
        encodedPolyline = in.readString();
        startLocation = in.readParcelable(Coordinate.class.getClassLoader());
        endLocation = in.readParcelable(Coordinate.class.getClassLoader());
        name = in.readString();
    }

    public static final Creator<Route> CREATOR = new Creator<Route>() {
        @Override
        public Route createFromParcel(Parcel in) {
            return new Route(in);
        }

        @Override
        public Route[] newArray(int size) {
            return new Route[size];
        }
    };

    public String getRouteName(){
        return name;
    }

    public int getId() {
        return id;
    }

    public String getEncodedPolyline() {
        return encodedPolyline;
    }

    public Coordinate getStartLocation() {
        return startLocation;
    }

    public Coordinate getEndLocation() {
        return endLocation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(encodedPolyline);
        dest.writeParcelable(startLocation, flags);
        dest.writeParcelable(endLocation, flags);
        dest.writeString(name);
    }

    @SerializedName(ID)
    @DatabaseField(columnName = ID, id = true)
    private int id;
    @SerializedName(ENCODED_POLYLINE)
    @DatabaseField(columnName = ENCODED_POLYLINE)
    private String encodedPolyline;
    @SerializedName(START_LOCATION)
    @DatabaseField(columnName = START_LOCATION)
    private Coordinate startLocation;
    @SerializedName(END_LOCATION)
    @DatabaseField(columnName = END_LOCATION)
    private Coordinate endLocation;
    @SerializedName(ROUTE_NAME)
    @DatabaseField(columnName = ROUTE_NAME)
    private String name;
}

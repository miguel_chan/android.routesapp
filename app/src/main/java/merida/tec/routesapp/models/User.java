package merida.tec.routesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by miguelchan on 30/05/16.
 */
@DatabaseTable
public class User implements Parcelable {

    public static final String ID = "Id";
    public static final String EMAIL = "Email";
    public static final String PASSWORD = "Password";
    public static final String GENDER = "Gender";
    public static final String PHONE_NUMBER = "Phonenumber";

    public User(){}

    public User(int id, String email, String phoneNumber, String password, String gender) {
        this.id = id;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.gender = gender;
    }

    protected User(Parcel in) {
        id = in.readInt();
        email = in.readString();
        phoneNumber = in.readString();
        password = in.readString();
        gender = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(email);
        dest.writeString(phoneNumber);
        dest.writeString(password);
        dest.writeString(gender);
    }

    @DatabaseField(columnName = ID, id = true)
    @SerializedName(ID)
    private int id;
    @DatabaseField(columnName = EMAIL)
    @SerializedName(EMAIL)
    private String email;
    @DatabaseField(columnName = PHONE_NUMBER)
    @SerializedName(PHONE_NUMBER)
    private String phoneNumber;
    @DatabaseField(columnName = PASSWORD)
    @SerializedName(PASSWORD)
    private String password;
    @DatabaseField(columnName = GENDER)
    @SerializedName(GENDER)
    private String gender;
}

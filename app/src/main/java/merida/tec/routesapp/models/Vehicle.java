package merida.tec.routesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 31/05/16.
 */
public class Vehicle implements Parcelable {

    public static final String ID = "Id";
    public static final String PLATES = "Plates";
    public static final String BRAND = "Brand";
    public static final String ACCEPTS_CHILDREN = "Acceptschildren";

    public Vehicle() {
    }

    public Vehicle(int id, String plates, String brand, boolean acceptsChildren) {
        this.id = id;
        this.plates = plates;
        this.brand = brand;
        this.acceptsChildren = acceptsChildren;
    }

    protected Vehicle(Parcel in) {
        id = in.readInt();
        plates = in.readString();
        brand = in.readString();
        acceptsChildren = in.readByte() != 0;
    }

    public static final Creator<Vehicle> CREATOR = new Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };

    public boolean isAcceptsChildren() {
        return acceptsChildren;
    }

    public String getBrand() {
        return brand;
    }

    public String getPlates() {
        return plates;
    }

    public int getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(plates);
        dest.writeString(brand);
        dest.writeByte((byte) (acceptsChildren ? 1 : 0));
    }

    @SerializedName(ID)
    private int id;
    @SerializedName(PLATES)
    private String plates;
    @SerializedName(BRAND)
    private String brand;
    @SerializedName(ACCEPTS_CHILDREN)
    private boolean acceptsChildren;
}

package merida.tec.routesapp.modules;

import com.google.inject.AbstractModule;

import merida.tec.routesapp.database.handlers.commands.AddRouteCommandHandler;
import merida.tec.routesapp.database.handlers.commands.AddVehicleCommandHandler;
import merida.tec.routesapp.database.handlers.commands.DeleteVehicleCommandHandler;
import merida.tec.routesapp.database.handlers.impl.BaseAddRouteCommandHandler;
import merida.tec.routesapp.database.handlers.queries.FindRoutesQueryHandler;
import merida.tec.routesapp.database.handlers.queries.GetUserQueryHandler;
import merida.tec.routesapp.database.handlers.commands.InsertUserCommandHandler;
import merida.tec.routesapp.database.handlers.impl.BaseAddVehicleCommandHandler;
import merida.tec.routesapp.database.handlers.impl.BaseDeleteVehicleCommandHandler;
import merida.tec.routesapp.database.handlers.impl.BaseFindRoutesQueryHandler;
import merida.tec.routesapp.database.handlers.impl.BaseGetUserQueryHandler;
import merida.tec.routesapp.database.handlers.impl.BaseInsertUserCommandHandler;
import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.impl.RetrofitClient;
import merida.tec.routesapp.web.impl.RetrofitWebServiceClient;

/**
 * Created by miguelchan on 30/05/16.
 */
public class AppModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(RetrofitClient.class).toProvider(RetrofitClientProvider.class);
        bind(WebServiceClient.class).to(RetrofitWebServiceClient.class);

        //Commands
        bind(InsertUserCommandHandler.class).to(BaseInsertUserCommandHandler.class);
        bind(AddVehicleCommandHandler.class).to(BaseAddVehicleCommandHandler.class);
        bind(DeleteVehicleCommandHandler.class).to(BaseDeleteVehicleCommandHandler.class);
        bind(AddRouteCommandHandler.class).to(BaseAddRouteCommandHandler.class);

        //Queries
        bind(GetUserQueryHandler.class).to(BaseGetUserQueryHandler.class);
        bind(FindRoutesQueryHandler.class).to(BaseFindRoutesQueryHandler.class);
    }
}

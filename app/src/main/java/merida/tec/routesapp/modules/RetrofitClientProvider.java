package merida.tec.routesapp.modules;

import javax.inject.Provider;

import merida.tec.routesapp.web.impl.RetrofitClient;

/**
 * Created by miguelchan on 30/05/16.
 */
/*package*/ class RetrofitClientProvider implements Provider<RetrofitClient>{


    @Override
    public RetrofitClient get() {
        RetrofitClient client = RetrofitClient.Factory.getClient();
        return client;
    }
}

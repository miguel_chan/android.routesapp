package merida.tec.routesapp.services;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.appengine.repackaged.com.google.api.client.util.Lists;

import java.util.List;

import javax.inject.Inject;

import merida.tec.routesapp.controllers.RoutesController;
import merida.tec.routesapp.models.Coordinate;
import merida.tec.routesapp.models.Route;
import merida.tec.routesapp.models.User;
import merida.tec.routesapp.utils.ObjectUtils;
import roboguice.service.RoboIntentService;

/**
 * Created by miguelchan on 03/06/16.
 */
public class RoutesService extends RoboIntentService {

    public static final String BROADCAST_ACTION_CREATE_ROUTE = "BACreateRoute";
    public static final String BROADCAST_ACTION_FETCH_ROUTES = "BAFetchRoutes";

    public static final String BROADCAST_EXTRA_ROUTES = "ExtraRoutes";
    public static final String BROADCAST_EXTRA_ROUTE_CREATED = "ExtraRouteCreated";

    private static final String ACTION_CREATE_ROUTE = "CreateRoute";
    private static final String ACTION_FETCH_ROUTES = "FetchRoutes";

    private static final String EXTRA_START_COORDINATE = "StartCoordinate";
    private static final String EXTRA_END_COORDINATE = "EndCoordinate";
    private static final String EXTRA_ROUTE_NAME = "RouteName";
    private static final String EXTRA_POLYLINE = "Polyline";
    private static final String EXTRA_USER = "User";

    private static final String TAG = RoutesService.class.getSimpleName();

    @Inject
    private RoutesController mRoutesController;

    public static class Builder{

        public static Builder fetchRoutes(Context context){
            Builder builder = new Builder(context);
            builder.action = ACTION_FETCH_ROUTES;
            return builder;
        }

        public static Builder createRoute(Context context){
            Builder builder = new Builder(context);
            builder.action = ACTION_CREATE_ROUTE;
            return builder;
        }

        public Builder setUser(User user){
            this.user = user;
            return this;
        }

        public Builder setRouteName(String routeName){
            this.routeName = routeName;
            return this;
        }

        public Builder setCoordinates(Coordinate startCoordinate, Coordinate endCoordinate){
            this.startCoordinate = startCoordinate;
            this.endCoordinate = endCoordinate;
            return this;
        }

        public Builder setPolyline(String encodedPolyline){
            this.polyline = encodedPolyline;
            return this;
        }

        public Intent build(){
            Intent intent = new Intent(mContext, RoutesService.class);
            intent.setAction(action);
            if(ObjectUtils.isNotNull(startCoordinate)) {
                intent.putExtra(EXTRA_START_COORDINATE, startCoordinate);
            }
            if(ObjectUtils.isNotNull(endCoordinate)) {
                intent.putExtra(EXTRA_END_COORDINATE, endCoordinate);
            }
            if(ObjectUtils.isNotNull(routeName)) {
                intent.putExtra(EXTRA_ROUTE_NAME, routeName);
            }
            if(ObjectUtils.isNotNull(polyline)) {
                intent.putExtra(EXTRA_POLYLINE, polyline);
            }
            if(ObjectUtils.isNotNull(user)) {
                intent.putExtra(EXTRA_USER, user);
            }

            return intent;
        }

        public void buildAndStart(){
            Intent intent = build();
            mContext.startService(intent);
        }

        private Builder(Context context){
            mContext = context;
        }

        private User user;
        private String routeName;
        private Coordinate startCoordinate;
        private Coordinate endCoordinate;
        private String polyline;
        private String action;
        private Context mContext;

    }

    public RoutesService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();

        if(ACTION_CREATE_ROUTE.equals(action)){
            createRoute(intent);
        }else if(ACTION_FETCH_ROUTES.equals(action)){
            fetchRoutes(intent);
        }
    }

    private void fetchRoutes(Intent intent){
        List<Route> routes;
        if(intent.hasExtra(EXTRA_USER)){
            User user = intent.getParcelableExtra(EXTRA_USER);
            routes = mRoutesController.getRoutes(user);
        }else{
            routes = mRoutesController.getRoutes();
        }

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_FETCH_ROUTES);
        broadcastIntent.putParcelableArrayListExtra(BROADCAST_EXTRA_ROUTES, Lists.newArrayList(routes));
        sendLocalBroadcast(broadcastIntent);
    }

    private void createRoute(Intent intent){
        Coordinate startCoordinate = intent.getParcelableExtra(EXTRA_START_COORDINATE);
        Coordinate endCoordinate = intent.getParcelableExtra(EXTRA_END_COORDINATE);
        String routeName = intent.getStringExtra(EXTRA_ROUTE_NAME);
        String polyline = intent.getStringExtra(EXTRA_POLYLINE);

        boolean created = mRoutesController.addRoute(
                startCoordinate,
                endCoordinate,
                polyline,
                routeName
        );

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_CREATE_ROUTE);
        broadcastIntent.putExtra(BROADCAST_EXTRA_ROUTE_CREATED, created);

        sendLocalBroadcast(broadcastIntent);
    }

    private void sendLocalBroadcast(Intent intent){
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}

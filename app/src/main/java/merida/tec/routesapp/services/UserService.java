package merida.tec.routesapp.services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import org.roboguice.shaded.goole.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

import merida.tec.routesapp.controllers.UserController;
import merida.tec.routesapp.models.User;
import roboguice.service.RoboIntentService;

/**
 * Created by miguelchan on 30/05/16.
 */
public class UserService extends RoboIntentService {

    public static final String EXTRA_USER = "ExtraUser";
    public static final String BROADCAST_ACTION_CREATION = "BroadcastCreation";
    public static final String BROADCAST_EXTRA_USER_CREATED = "UserCreatedExtra";

    public static final String ACTION_LOG_IN = "ActionLogIn";
    public static final String EXTRA_USER_NAME = "UserName";
    public static final String EXTRA_PASSWORD = "Password";
    public static final String BROADCAST_ACTION_LOG_IN = "BroadcastLogIn";
    public static final String BROADCAST_EXTRA_LOGGED_IN = "LoggedIn";

    public static final String ACTION_GET_USERS = "GetUsers";
    public static final String BROADCAST_ACTION_GET_USERS = "BAGetUsers";
    public static final String BROADCAST_EXTRA_USERS = "USers";

    private static final String TAG = UserService.class.getSimpleName();

    @Inject
    private UserController mUserController;

    public UserService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();

        if(ACTION_LOG_IN.equals(action)){
            logIn(intent);
        } else if (ACTION_GET_USERS.equals(action)){
            getUsers(intent);
        } else {
            createUser(intent);
        }
    }

    private void getUsers(Intent intent){
        List<User> users = mUserController.getUsers();

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_GET_USERS);
        broadcastIntent.putParcelableArrayListExtra(BROADCAST_EXTRA_USERS, Lists.newArrayList(users));
        sendLocalBroadcast(broadcastIntent);
    }

    private void logIn(Intent intent){
        String userName = intent.getStringExtra(EXTRA_USER_NAME);
        String password = intent.getStringExtra(EXTRA_PASSWORD);

        boolean loggedIn = mUserController.logIn(userName, password);

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_LOG_IN);
        broadcastIntent.putExtra(BROADCAST_EXTRA_LOGGED_IN, loggedIn);
        sendLocalBroadcast(broadcastIntent);
    }

    private void createUser(Intent intent){
        User user = intent.getParcelableExtra(EXTRA_USER);

        boolean created = mUserController.createUser(user);

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_CREATION);
        broadcastIntent.putExtra(BROADCAST_EXTRA_USER_CREATED, created);
        sendLocalBroadcast(broadcastIntent);
    }

    private void sendLocalBroadcast(Intent broadcastIntent) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }
}

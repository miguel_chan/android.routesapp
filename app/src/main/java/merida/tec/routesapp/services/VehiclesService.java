package merida.tec.routesapp.services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import org.roboguice.shaded.goole.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

import merida.tec.routesapp.controllers.VehiclesController;
import merida.tec.routesapp.models.User;
import merida.tec.routesapp.models.Vehicle;
import roboguice.service.RoboIntentService;

/**
 * Created by miguelchan on 31/05/16.
 */
public class VehiclesService extends RoboIntentService {

    public static final String ACTION_GET_VEHICLES = "GetVehicles";
    public static final String BROADCAST_ACTION_GET_VEHICLES = "BAGetVehicles";

    public static final String ACTION_CREATE_VEHICLE = "CreateVehicle";
    public static final String BROADCAST_ACTION_VEHICLE_CREATED = "BACreated";
    public static final String BROADCAST_EXTRA_VEHICLE_CREATED = "BAExtraCreated";
    public static final String EXTRA_BRAND = "Brand";
    public static final String EXTRA_PLATES = "Plates";
    public static final String EXTRA_ACCEPTS = "AcceptsChildren";

    public static final String ACTION_DELETE_VEHICLE = "DeleteVehicle";
    public static final String EXTRA_VEHICLE = "ExtraVehicle";
    public static final String EXTRA_USER = "ExtraUser";

    public static final String BROADCAST_ACTION_DELETED_VEHICLE = "BADeleteVehicle";
    public static final String BROADCAST_EXTRA_DELETED_VEHICLE = "BAXDeletedVehicle";

    public static final String BROADCAST_EXTRA_VEHICLES = "BAExtraVehicles";

    private static final String TAG = VehiclesService.class.getSimpleName();

    @Inject
    private VehiclesController mVehiclesController;

    public VehiclesService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if(ACTION_GET_VEHICLES.equals(action)){
            getVehicles(intent);
        }else if(ACTION_CREATE_VEHICLE.equals(action)){
            addVehicle(intent);
        }else if(ACTION_DELETE_VEHICLE.equals(action)){
            deleteVehicle(intent);
        }
    }

    private void deleteVehicle(Intent intent){
        Vehicle vehicle = intent.getParcelableExtra(EXTRA_VEHICLE);

        boolean deleted;
        if(intent.hasExtra(EXTRA_USER)){
            User user = intent.getParcelableExtra(EXTRA_USER);
            deleted = mVehiclesController.deleteVehicle(user, vehicle);
        }else{
            deleted = mVehiclesController.deleteVehicle(vehicle);
        }

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_DELETED_VEHICLE);
        broadcastIntent.putExtra(BROADCAST_EXTRA_DELETED_VEHICLE, deleted);
        sendLocalBroadcast(broadcastIntent);
    }

    private void addVehicle(Intent intent){
        String brand = intent.getStringExtra(EXTRA_BRAND);
        String plates = intent.getStringExtra(EXTRA_PLATES);
        boolean accepts = intent.getBooleanExtra(EXTRA_ACCEPTS, false);

        boolean created = mVehiclesController.addVehicle(brand, plates, accepts);

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_VEHICLE_CREATED);
        broadcastIntent.putExtra(BROADCAST_EXTRA_VEHICLE_CREATED, created);
        sendLocalBroadcast(broadcastIntent);
    }

    private void sendLocalBroadcast(Intent intent){
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void getVehicles(Intent intent){
        List<Vehicle> vehicles;
        if(intent.hasExtra(EXTRA_USER)){
            User user = intent.getParcelableExtra(EXTRA_USER);
            vehicles = mVehiclesController.getVehicles(user);
        }else{
            vehicles = mVehiclesController.getVehicles();
        }

        Intent broadcastIntent = new Intent(BROADCAST_ACTION_GET_VEHICLES);
        broadcastIntent.putParcelableArrayListExtra(BROADCAST_EXTRA_VEHICLES, Lists.newArrayList(vehicles));

        sendLocalBroadcast(intent);
    }
}

package merida.tec.routesapp.utils;

import android.support.v4.app.Fragment;

/**
 * Created by miguelchan on 30/05/16.
 */
public interface FragmentHandler {

    void addFragment(Fragment fragment, String tag, boolean addToBackStack);

}

package merida.tec.routesapp.utils;

import java.util.List;

/**
 * Created by miguelchan on 31/05/16.
 */
public class ListUtils {

    public static boolean isValidList(List list){
        return ObjectUtils.isNotNull(list) && list.size() > 0;
    }

    private ListUtils(){}
}

package merida.tec.routesapp.utils;

/**
 * Created by miguelchan on 30/05/16.
 */
public class ObjectUtils {

    public static boolean isNotNull(Object object){
        return null != object;
    }

    public static boolean isNull(Object object){
        return null == object;
    }

    public static <T> T firstNotNull(T t1, T t2){
        return isNull(t1) ? t2 : t1;
    }

    private ObjectUtils(){}
}

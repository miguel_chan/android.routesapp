package merida.tec.routesapp.utils;

import android.view.View;

/**
 * Created by miguelchan on 05/06/16.
 */
public interface OnViewHolderClickListener {

    void onViewHolderClick(View view, int position);

}

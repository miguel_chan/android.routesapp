package merida.tec.routesapp.utils;

import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.internal.PolylineEncoding;
import com.google.maps.model.EncodedPolyline;
import com.google.maps.model.LatLng;

import org.roboguice.shaded.goole.common.base.Function;
import org.roboguice.shaded.goole.common.collect.FluentIterable;

import java.util.List;

/**
 * Created by miguelchan on 04/06/16.
 */
public class PolylineUtils {

    public static boolean equals(PolylineOptions lhs, PolylineOptions rhs){
        List<com.google.android.gms.maps.model.LatLng> tempLhs = lhs.getPoints();
        List<com.google.android.gms.maps.model.LatLng> tempRhs = rhs.getPoints();
        return equalPoints(tempLhs, tempRhs);
    }

    public static String encodePolyline(PolylineOptions options){
        List<com.google.android.gms.maps.model.LatLng> pos = options.getPoints();
        List<LatLng> latLngs = toLatLng(pos);
        return PolylineEncoding.encode(latLngs);
    }

    public static boolean equals(Polyline lhs, Polyline rhs){
        List<com.google.android.gms.maps.model.LatLng> tempLhs = lhs.getPoints();
        List<com.google.android.gms.maps.model.LatLng> tempRhs = rhs.getPoints();
        return equalPoints(tempLhs, tempRhs);
    }

    public static boolean equalPoints(List<com.google.android.gms.maps.model.LatLng> lhs,
                                      List<com.google.android.gms.maps.model.LatLng> rhs){
        List<LatLng> tempLhs = toLatLng(lhs);
        List<LatLng> tempRhs = toLatLng(rhs);

        String lhsString = PolylineEncoding.encode(tempLhs);
        String rhsString = PolylineEncoding.encode(tempRhs);

        return rhsString.equals(lhsString);
    }

    public static List<com.google.android.gms.maps.model.LatLng> toLatLng(String encodedPolyline){
        List<LatLng> latLngs = PolylineEncoding.decode(encodedPolyline);
        return FluentIterable.from(latLngs).transform(new Function<LatLng, com.google.android.gms.maps.model.LatLng>() {
            @Override
            public com.google.android.gms.maps.model.LatLng apply(LatLng latLng) {
                return new com.google.android.gms.maps.model.LatLng(latLng.lat, latLng.lng);
            }
        }).toList();
    }

    public static List<LatLng> toLatLng(List<com.google.android.gms.maps.model.LatLng> latLngList){
        return FluentIterable.from(latLngList).transform(new Function<com.google.android.gms.maps.model.LatLng, LatLng>() {
            @Override
            public LatLng apply(com.google.android.gms.maps.model.LatLng latLng) {
                return new LatLng(latLng.latitude, latLng.longitude);
            }
        }).toList();
    }

}

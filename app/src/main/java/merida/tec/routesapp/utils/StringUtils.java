package merida.tec.routesapp.utils;

/**
 * Created by miguelchan on 30/05/16.
 */
public class StringUtils {

    public static final boolean isValidString(String string){
        return ObjectUtils.isNotNull(string) && string.length() > 0;
    }

    private StringUtils(){}
}

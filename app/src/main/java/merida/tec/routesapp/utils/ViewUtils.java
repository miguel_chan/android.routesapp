package merida.tec.routesapp.utils;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.view.View;

/**
 * Created by miguelchan on 31/05/16.
 */
public class ViewUtils {

    public static <T extends View> T findViewById(View view, @IdRes int resourceId){
        return ( T ) view.findViewById(resourceId);
    }

    public static <T extends View> T findViewById(Activity activity, @IdRes int resourceId){
        return ( T ) activity.findViewById(resourceId);
    }
    private ViewUtils(){}
}

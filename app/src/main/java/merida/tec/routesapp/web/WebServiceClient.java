package merida.tec.routesapp.web;

import merida.tec.routesapp.web.requests.AddRouteRequest;
import merida.tec.routesapp.web.requests.CreateUserRequest;
import merida.tec.routesapp.web.requests.CreateUserVehicleRequest;
import merida.tec.routesapp.web.requests.DeleteVehicleRequest;
import merida.tec.routesapp.web.requests.GetUserRequest;
import merida.tec.routesapp.web.requests.GetUserRoutesRequest;
import merida.tec.routesapp.web.requests.GetUserVehiclesRequest;
import merida.tec.routesapp.web.requests.GetUsersRequest;
import merida.tec.routesapp.web.requests.LogInRequest;
import merida.tec.routesapp.web.responses.AddRouteResponse;
import merida.tec.routesapp.web.responses.CreateUserResponse;
import merida.tec.routesapp.web.responses.CreateUserVehicleResponse;
import merida.tec.routesapp.web.responses.DeleteVehicleResponse;
import merida.tec.routesapp.web.responses.GetUserResponse;
import merida.tec.routesapp.web.responses.GetUserRoutesResponse;
import merida.tec.routesapp.web.responses.GetUserVehiclesResponse;
import merida.tec.routesapp.web.responses.GetUsersResponse;
import merida.tec.routesapp.web.responses.LogInResponse;

/**
 * Created by miguelchan on 30/05/16.
 */
public interface WebServiceClient {

    CreateUserResponse createUse(CreateUserRequest request);
    CreateUserVehicleResponse createUserVehicle(CreateUserVehicleRequest request);
    LogInResponse logIn(LogInRequest request);
    GetUserResponse getUser(GetUserRequest request);
    GetUsersResponse getUsers(GetUsersRequest request);
    GetUserVehiclesResponse getUserVehicles(GetUserVehiclesRequest request);
    DeleteVehicleResponse deleteVehicle(DeleteVehicleRequest request);
    GetUserRoutesResponse getUserRoutes(GetUserRoutesRequest request);
    AddRouteResponse addRoute(AddRouteRequest request);

}

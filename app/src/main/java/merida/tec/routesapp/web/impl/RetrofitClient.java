package merida.tec.routesapp.web.impl;

import merida.tec.routesapp.web.requests.AddRouteRequest;
import merida.tec.routesapp.web.requests.CreateUserRequest;
import merida.tec.routesapp.web.requests.CreateUserVehicleRequest;
import merida.tec.routesapp.web.requests.LogInRequest;
import merida.tec.routesapp.web.responses.AddRouteResponse;
import merida.tec.routesapp.web.responses.CreateUserResponse;
import merida.tec.routesapp.web.responses.CreateUserVehicleResponse;
import merida.tec.routesapp.web.responses.DeleteVehicleResponse;
import merida.tec.routesapp.web.responses.GetUserResponse;
import merida.tec.routesapp.web.responses.GetUserRoutesResponse;
import merida.tec.routesapp.web.responses.GetUserVehiclesResponse;
import merida.tec.routesapp.web.responses.GetUsersResponse;
import merida.tec.routesapp.web.responses.LogInResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by miguelchan on 30/05/16.
 */
public interface RetrofitClient {

    class Factory {
        public static RetrofitClient getClient() {
            final String baseUrl = "http://routesapi.esy.es";

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient
                    .Builder()
                    .addInterceptor(interceptor)
                    .build();

            Retrofit retrofit = new Retrofit
                    .Builder()
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            return retrofit.create(RetrofitClient.class);
        }
    }

    @GET("/Api/Users")
    Call<GetUsersResponse> getUsers(@Query("pageSize") int pageSize,
                                    @Query("pageNumber") int pageNumber);

    @POST("/Api/Users")
    Call<CreateUserResponse> createUser(@Body CreateUserRequest request);

    @POST("/Api/Users/{UserId}/Vehicles")
    Call<CreateUserVehicleResponse> createUserVehicle(@Body CreateUserVehicleRequest request,
                                                      @Path("UserId") int userId);

    @POST("/Api/Users/LogIn")
    Call<LogInResponse> logIn(@Body LogInRequest request);

    @GET("/Api/Users/{UserId}")
    Call<GetUserResponse> getUser(@Path("UserId") int userId);

    @GET("/Api/Users/{UserId}/Vehicles")
    Call<GetUserVehiclesResponse> getUserVehicles(@Path("UserId") int userId,
                                                  @Query("pageSize") int pageSize,
                                                  @Query("pageNumber") int pageNumber);

    @DELETE("/Api/Users/{UserId/Vehicles/{VehicleId}")
    Call<DeleteVehicleResponse> deleteVehicle(@Path("UserId") int userId,
                                              @Path("VehicleId") int vehicleId);

    @GET("/Api/Users/{UserId}/Routes")
    Call<GetUserRoutesResponse> getUserRoutes(@Path("UserId") int userId);

    @POST("/Api/Users/{UserId}/Routes")
    Call<AddRouteResponse> addRoute(@Path("UserId") int userId,
                                    @Body AddRouteRequest request);
}

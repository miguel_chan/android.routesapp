package merida.tec.routesapp.web.impl;

import java.io.IOException;

import javax.inject.Inject;

import merida.tec.routesapp.web.WebServiceClient;
import merida.tec.routesapp.web.requests.AddRouteRequest;
import merida.tec.routesapp.web.requests.CreateUserRequest;
import merida.tec.routesapp.web.requests.CreateUserVehicleRequest;
import merida.tec.routesapp.web.requests.DeleteVehicleRequest;
import merida.tec.routesapp.web.requests.GetUserRequest;
import merida.tec.routesapp.web.requests.GetUserRoutesRequest;
import merida.tec.routesapp.web.requests.GetUserVehiclesRequest;
import merida.tec.routesapp.web.requests.GetUsersRequest;
import merida.tec.routesapp.web.requests.LogInRequest;
import merida.tec.routesapp.web.responses.AddRouteResponse;
import merida.tec.routesapp.web.responses.CreateUserResponse;
import merida.tec.routesapp.web.responses.CreateUserVehicleResponse;
import merida.tec.routesapp.web.responses.DeleteVehicleResponse;
import merida.tec.routesapp.web.responses.GetUserResponse;
import merida.tec.routesapp.web.responses.GetUserRoutesResponse;
import merida.tec.routesapp.web.responses.GetUserVehiclesResponse;
import merida.tec.routesapp.web.responses.GetUsersResponse;
import merida.tec.routesapp.web.responses.LogInResponse;
import retrofit2.Call;

/**
 * Created by miguelchan on 30/05/16.
 */
public class RetrofitWebServiceClient implements WebServiceClient {

    private RetrofitClient mClient;

    @Inject
    public RetrofitWebServiceClient(RetrofitClient client){
        mClient = client;
    }

    @Override
    public CreateUserResponse createUse(CreateUserRequest request) {
        CreateUserResponse response = null;

        Call<CreateUserResponse> responseCall = mClient.createUser(request);

        try {
            response = responseCall.execute().body();
        }catch(Exception e){
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public CreateUserVehicleResponse createUserVehicle(CreateUserVehicleRequest request) {
        CreateUserVehicleResponse response = null;

        int userId = request.getUserId();
        Call<CreateUserVehicleResponse> responseCall = mClient.createUserVehicle(request, userId);

        try{
            response = responseCall.execute().body();
        }catch(Exception e){
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public LogInResponse logIn(LogInRequest request) {
        LogInResponse response = null;

        Call<LogInResponse> responseCall = mClient.logIn(request);

        try {
            response = responseCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public GetUserResponse getUser(GetUserRequest request) {
        GetUserResponse response = null;

        int userId = request.getUserId();
        Call<GetUserResponse> responseCall = mClient.getUser(userId);

        try {
            response = responseCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public GetUsersResponse getUsers(GetUsersRequest request) {
        GetUsersResponse response = null;

        int pageSize = request.getPageSize();
        int pageNumber = request.getPageNumber();

        Call<GetUsersResponse> responseCall = mClient.getUsers(pageSize, pageNumber);

        try {
            response = responseCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public GetUserVehiclesResponse getUserVehicles(GetUserVehiclesRequest request) {
        GetUserVehiclesResponse response = null;

        int pageSize = request.getPageSize();
        int pageNumber = request.getPageNumber();
        int userId = request.getUserId();

        Call<GetUserVehiclesResponse> responseCall = mClient.getUserVehicles(userId,
                pageSize,
                pageNumber);

        try {
            response = responseCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public DeleteVehicleResponse deleteVehicle(DeleteVehicleRequest request) {
        DeleteVehicleResponse response = null;

        int userId = request.getUserId();
        int vehicleId = request.getVehicleId();

        Call<DeleteVehicleResponse> responseCall = mClient.deleteVehicle(userId, vehicleId);

        try {
            response = responseCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public GetUserRoutesResponse getUserRoutes(GetUserRoutesRequest request) {
        GetUserRoutesResponse response = null;

        int userId = request.getUserId();

        Call<GetUserRoutesResponse> responseCall = mClient.getUserRoutes(userId);

        try {
            response = responseCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public AddRouteResponse addRoute(AddRouteRequest request) {
        AddRouteResponse response = null;

        Call<AddRouteResponse> responseCall = mClient.addRoute(request.getUserId(), request);

        try {
            response = responseCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }
}

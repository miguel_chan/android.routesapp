package merida.tec.routesapp.web.requests;

import com.google.gson.annotations.SerializedName;

import merida.tec.routesapp.models.Coordinate;

/**
 * Created by miguelchan on 03/06/16.
 */
public class AddRouteRequest {

    public AddRouteRequest(int userId,
                           String name,
                           Coordinate startLocation,
                           Coordinate endLocation,
                           String encodedPolyline) {
        this.userId = userId;
        this.name = name;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.encodedPolyline = encodedPolyline;
    }

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public Coordinate getStartLocation() {
        return startLocation;
    }

    public Coordinate getEndLocation() {
        return endLocation;
    }

    public String getEncodedPolyline() {
        return encodedPolyline;
    }

    private int userId;
    @SerializedName("Name")
    private String name;
    @SerializedName("StartLocation")
    private Coordinate startLocation;
    @SerializedName("EndLocation")
    private Coordinate endLocation;
    @SerializedName("EncodedPolyline")
    private String encodedPolyline;
}

package merida.tec.routesapp.web.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 30/05/16.
 */
public class CreateUserRequest {

    public CreateUserRequest(String phoneNumber,
                             String email,
                             String password,
                             String gender,
                             String imagePath) {
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.imagePath = imagePath;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getGender() {
        return gender;
    }

    public String getImagePath() {
        return imagePath;
    }

    @SerializedName("PhoneNumber")
    private String phoneNumber;
    @SerializedName("Email")
    private String email;
    @SerializedName("Password")
    private String password;
    @SerializedName("Gender")
    private String gender;
    private String imagePath;
}

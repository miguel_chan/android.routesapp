package merida.tec.routesapp.web.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 30/05/16.
 */
public class CreateUserVehicleRequest {

    public CreateUserVehicleRequest(String plates, String brand, boolean acceptsChildren, int userId) {
        this.plates = plates;
        this.brand = brand;
        this.acceptsChildren = acceptsChildren;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public String getPlates() {
        return plates;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isAcceptsChildren() {
        return acceptsChildren;
    }

    private int userId;
    @SerializedName("Plates")
    private String plates;
    @SerializedName("Brand")
    private String brand;
    @SerializedName("AcceptsChildren")
    private boolean acceptsChildren;
}

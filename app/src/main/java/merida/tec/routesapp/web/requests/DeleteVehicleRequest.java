package merida.tec.routesapp.web.requests;

/**
 * Created by miguelchan on 03/06/16.
 */
public class DeleteVehicleRequest {

    public DeleteVehicleRequest(int userId, int vehicleId) {
        this.userId = userId;
        this.vehicleId = vehicleId;
    }

    public int getUserId() {
        return userId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    private int userId;
    private int vehicleId;
}

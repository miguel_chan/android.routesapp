package merida.tec.routesapp.web.requests;

/**
 * Created by miguelchan on 30/05/16.
 */
public class GetUserRequest {

    public GetUserRequest(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    private int userId;
}

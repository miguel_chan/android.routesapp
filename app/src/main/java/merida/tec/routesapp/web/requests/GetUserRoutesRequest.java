package merida.tec.routesapp.web.requests;

/**
 * Created by miguelchan on 03/06/16.
 */
public class GetUserRoutesRequest {

    public GetUserRoutesRequest(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    private int userId;
}

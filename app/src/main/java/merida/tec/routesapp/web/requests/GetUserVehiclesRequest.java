package merida.tec.routesapp.web.requests;

/**
 * Created by miguelchan on 31/05/16.
 */
public class GetUserVehiclesRequest {

    public GetUserVehiclesRequest(int userId, int pageSize, int pageNumber) {
        this.userId = userId;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }

    public int getUserId() {
        return userId;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    private int userId;
    private int pageSize;
    private int pageNumber;
}

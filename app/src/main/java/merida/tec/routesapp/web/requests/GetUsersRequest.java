package merida.tec.routesapp.web.requests;

/**
 * Created by miguelchan on 31/05/16.
 */
public class GetUsersRequest {

    public GetUsersRequest(int pageSize, int pageNumber) {
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    private int pageSize;
    private int pageNumber;
}

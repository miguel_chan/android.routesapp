package merida.tec.routesapp.web.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 30/05/16.
 */
public class LogInRequest {

    public LogInRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    @SerializedName("UserName")
    private String userName;
    @SerializedName("Password")
    private String password;
}

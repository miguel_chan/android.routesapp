package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 03/06/16.
 */
public class AddRouteResponse {

    public AddRouteResponse(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    @SerializedName("Id")
    private int id;
    @SerializedName("Message")
    private String message;
}

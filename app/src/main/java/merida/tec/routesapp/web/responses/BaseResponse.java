package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 30/05/16.
 */
public class BaseResponse {

    public BaseResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @SerializedName("Message")
    private String message;
}

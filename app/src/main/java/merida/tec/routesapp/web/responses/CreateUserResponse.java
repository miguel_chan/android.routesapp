package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 30/05/16.
 */
public class CreateUserResponse extends BaseResponse{

    public CreateUserResponse(String message, int id) {
        super(message);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @SerializedName("Id")
    private int id;
}

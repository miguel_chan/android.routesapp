package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 03/06/16.
 */
public class DeleteVehicleResponse {

    public DeleteVehicleResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    @SerializedName("Success")
    private boolean success;
    @SerializedName("Message")
    private String message;
}

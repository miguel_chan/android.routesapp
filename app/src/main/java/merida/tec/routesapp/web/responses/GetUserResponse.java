package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 30/05/16.
 */
public class GetUserResponse extends BaseResponse {

    public GetUserResponse(String message, User user) {
        super(message);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @SerializedName("User")
    private User user;
}

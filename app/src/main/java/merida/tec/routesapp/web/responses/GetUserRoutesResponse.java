package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import merida.tec.routesapp.models.Route;

/**
 * Created by miguelchan on 03/06/16.
 */
public class GetUserRoutesResponse {

    public GetUserRoutesResponse(List<Route> routes, String message) {
        this.routes = routes;
        this.message = message;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public String getMessage() {
        return message;
    }

    @SerializedName("Routes")
    private List<Route> routes;
    @SerializedName("Message")
    private String message;
}

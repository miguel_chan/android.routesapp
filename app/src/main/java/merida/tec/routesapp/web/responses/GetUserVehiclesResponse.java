package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import merida.tec.routesapp.models.Vehicle;

/**
 * Created by miguelchan on 31/05/16.
 */
public class GetUserVehiclesResponse extends BaseResponse {

    public GetUserVehiclesResponse(String message, int totalCount, List<Vehicle> vehicles) {
        super(message);
        this.totalCount = totalCount;
        this.vehicles = vehicles;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    @SerializedName("TotalCount")
    private int totalCount;
    @SerializedName("Vehicles")
    private List<Vehicle> vehicles;
}

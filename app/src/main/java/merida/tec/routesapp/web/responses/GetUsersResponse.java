package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import merida.tec.routesapp.models.User;

/**
 * Created by miguelchan on 31/05/16.
 */
public class GetUsersResponse extends BaseResponse {

    public GetUsersResponse(String message, int totalCount, List<User> users) {
        super(message);
        this.totalCount = totalCount;
        this.users = users;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public List<User> getUsers() {
        return users;
    }

    @SerializedName("TotalCount")
    private int totalCount;
    @SerializedName("Users")
    private List<User> users;
}

package merida.tec.routesapp.web.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelchan on 30/05/16.
 */
public class LogInResponse extends BaseResponse{

    public LogInResponse(String message, int id, boolean loggedIn) {
        super(message);
        this.id = id;
        this.loggedIn = loggedIn;
    }

    public int getId() {
        return id;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    @SerializedName("UserId")
    private int id;
    @SerializedName("LoggedIn")
    private boolean loggedIn;
}
